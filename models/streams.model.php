<?php
ini_set('memory_limit', '4095M'); 

class Streams{
	
	public $stream_id;
	public $stream_name;
	public $stream_number;
	public $stream_status;
	

	public function __construct($stream_id, $stream_name, $stream_number, $stream_status) {
		$this->stream_id     = $stream_id;
		$this->stream_name   = $stream_name;
		$this->stream_number = $stream_number;
		$this->stream_status = $stream_status;
	}

	public function LastInsert($DbName)
	{
		$db 	= new SQLite3($DbName);
		//$query 	= "SELECT last_insert_rowid()";
		//$query_results = $db->query($query);

		$LastInsert = $db->lastInsertRowID();

		return $LastInsert; 
	}
	
	public static function YourStream($DbName)
	{
	
		$db = new SQLite3($DbName);
		$query = "SELECT * FROM streams where PEER_ID = '0' ";
		$query_results = $db->query($query);
	
		if (!$query_results) die("Cannot execute query.");
	
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
	
		$db->close();
		return $results;
	}

	public static function YourStreamStatus($DbName, $status)
	{
	
		$db = new SQLite3($DbName);
		$query = "SELECT * FROM streams where PEER_ID = '0' and STREAM_STATUS ='" . $status . "'" ;
		$query_results = $db->query($query);
	
		if (!$query_results) die("Cannot execute query.");
	
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
	
		$db->close();
		return $results;
	}	

	public static function YourStreamInCat($DbName, $Cat){
	
		$db = new SQLite3($DbName);
		$query = "SELECT * FROM streams where PEER_ID = '0' AND STREAM_CAT = '$Cat' ";
		$query_results = $db->query($query);
	
		if (!$query_results) die("Cannot execute query.");
	
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
	
		$db->close();
		return $results;
	}	

	public static function PeerStreams($DbName)
	{
	
		$db = new SQLite3($DbName);

		$query = "SELECT * FROM streams where PEER_ID > '0' ";
		$query_results = $db->query($query);
	
		if (!$query_results) die("Cannot execute query.");
	
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
	
		$db->close();
		return $results;
	}

	public static function FindPeerStreams($DbName, $StreamID)
	{
	
		$db = new SQLite3($DbName);

		$query = "SELECT * FROM streams where STREAM_MAP = '$StreamID' ";
		$query_results = $db->query($query);
	
		if (!$query_results) die("Cannot execute query.");
	
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
	
		$db->close();
		return $results;
	}		

	public static function PeerOnlineStreams($DbName)
	{
	
		$db = new SQLite3($DbName);

		$query = "SELECT * FROM streams where PEER_ID > '0' AND STREAM_STATUS = 'ON'";
		$query_results = $db->query($query);
	
		if (!$query_results) die("Cannot execute query.");
	
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
	
		$db->close();
		return $results;
	}		

	public static function PeerIGNStreams($DbName)
	{
	
		$db = new SQLite3($DbName);

		$query = "SELECT * FROM streams where PEER_ID > '0' AND STREAM_IGN IS NOT NULL";
		$query_results = $db->query($query);
	
		if (!$query_results) die("Cannot execute query.");
	
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
	
		$db->close();
		return $results;
	}

	public static function PeerMAPStreams($DbName)
	{
	
		$db = new SQLite3($DbName);

		$query = "SELECT * FROM streams where PEER_ID > '0' AND STREAM_MAP IS NOT NULL";
		$query_results = $db->query($query);
	
		if (!$query_results) die("Cannot execute query.");
	
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
	
		$db->close();
		return $results;
	}	

	public static function GetNumbStreamByStatusInCat($DbName, $Cat, $Status)
	{
		$db = new SQLite3($DbName);

		if ($Status = 'ON')
		{
			//Active Streams are also Online, select all in status different from OFF
			$Status = 'OFF';
			$query = "SELECT * FROM streams where PEER_ID = '0' AND STREAM_CAT = '$Cat' AND STREAM_STATUS != '$Status' ";
		}
		else
		{
			$query = "SELECT * FROM streams where PEER_ID = '0' AND STREAM_CAT = '$Cat' AND STREAM_STATUS = '$Status' ";
		}

		$query_results = $db->query($query);
	
		if (!$query_results) die("Cannot execute query in GetNumbStreamByStatusInCat");
	
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
	
		$db->close();
		return $results;		
	}	

	public static function AssignStream($DbName, $Cat)
	{
	
		$db = new SQLite3($DbName);
		if ($Cat)
		{
			if ($Cat == "un")
			{
				$query = "SELECT * FROM streams where PEER_ID = '0' AND STREAM_CAT = '1' ";
			}	
			if ($Cat == "all")
			{
				$query = "SELECT * FROM streams where PEER_ID = '0'";
			}	
			if (($Cat != "un") AND ($Cat != "all"))		
			{
				$query = "SELECT * FROM streams where PEER_ID = '0' AND STREAM_CAT = '$Cat'";
			}		
		}
		else
		{
			if ($Cat == '0')
			{
				$query = "SELECT * FROM streams where PEER_ID = '0' AND STREAM_CAT = '1' ";
			}
			else
			{
				$query = "SELECT * FROM streams where PEER_ID = '0'";
			}
			
		}

		//echo $query . "<br>";

		$query_results = $db->query($query);
	
		if (!$query_results) die("Cannot execute query in AssignStream");
	
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
	
		$db->close();
		return $results;
	}	


	public static function all($DbName){
	
		$db = new SQLite3($DbName);
		$query = "SELECT * FROM streams";
		$query_results = $db->query($query);
	
		if (!$query_results) die("Cannot execute query.");
	
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
	
		$db->close();
		return $results;
	}
	
	public static function FindAllBackups($DbName)
	{
		$db = new SQLite3($DbName);
		//$query = "SELECT * FROM streams S, streams_backup B, peers P where (S.id=B.STREAM) and (P.id = B.PEER)" ;
		$query = "SELECT * FROM streams where stream_map IS NOT NULL ";
	
		$query_results = $db->query($query);
	
		if (!$query_results) die("Cannot execute query.");
	
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
		$db->close();
		return $results;
	}

	public function InsertStream($DbName, $PeerID, $StreamName, $StreamNumber)
	{
		$db = new SQLite3($DbName);
		$db->exec('INSERT INTO streams (stream_name, stream_number, peer_id) VALUES ("' . $StreamName . '","' . $StreamNumber .'","' . $PeerID . '")');
		$db->close();
	}

	public function IgnoreStream($DbName, $PeerID, $StreamID, $mode)
	{
		$db = new SQLite3($DbName);
		$query = "UPDATE streams SET STREAM_IGN ='". $mode ."' WHERE ID='". $StreamID . "'";
		//echo $query . "<br>";
		$db->exec($query);
		$db->close();

	}

	public function StreamSrc($DbName, $StreamID)
	{
			
		$db = new SQLite3($DbName);

		$query = "SELECT * FROM streams S, peers P where S.PEER_ID=P.ID and S.ID='" . $StreamID . "'";
		$query_results = $db->query($query);
		$results = array();
		
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
		foreach($results as $stream) {
			$StreamSrc = $stream["PEER_URL"] . $stream["STREAM_NUMBER"] . ".ts";
		}
		
		$db->close();
		
		return $StreamSrc;
	}	

	public function StreamStatus($DbName, $StreamID, $Status)
	{	
		$db = new SQLite3($DbName);
		$update_query = "UPDATE streams SET STREAM_STATUS='". $Status ."' WHERE ID='".$StreamID . "'";
		$db->exec($update_query);	
		$db->close();
	}	

	public function StreamVideoCodec($DbName, $StreamID, $Stream_VC)
	{	
		$db = new SQLite3($DbName);
		$update_query = "UPDATE streams SET STREAM_VC='". $Stream_VC ."' WHERE ID='".$StreamID . "'";
		$db->exec($update_query);	
		$db->close();
	}	

	public function StreamAudioCodec($DbName, $StreamID, $stream_AC)
	{	
		$db = new SQLite3($DbName);
		$update_query = "UPDATE streams SET STREAM_AC ='" . $stream_AC ."' WHERE ID='".$StreamID . "'";
		$db->exec($update_query);	
		$db->close();
	}	

	public function StreamBitRate($DbName, $StreamID, $StreamBitRate)
	{	
		$db = new SQLite3($DbName);
		$update_query = "UPDATE streams SET STREAM_QA ='" . $StreamBitRate ."' WHERE ID='".$StreamID . "'";
		$db->exec($update_query);	
		$db->close();
	}			

	public function GetName($DbName, $StreamID)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT STREAM_NAME FROM streams WHERE ID='" .$StreamID . "'" ;
		$query_results = $db->querySingle($query);	
		$db->close();	

		return $query_results;	
	}

	public function GetSource($DbName, $StreamID)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT SOURCE FROM streams WHERE ID='" . $StreamID . "'" ;
		$query_results = $db->querySingle($query);	
		$db->close();	

		return $query_results;	
	}	

	public function GetStreamPID($DbName, $StreamID)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT STREAM_PID FROM streams WHERE ID='" . $StreamID . "'" ;
		$query_results = $db->querySingle($query);	
		$db->close();	

		return $query_results;	
	}

	public function GetCategory($DbName, $StreamID)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT STREAM_CAT FROM streams WHERE ID='" .$StreamID . "'" ;
		$query_results = $db->querySingle($query);	
		$db->close();	

		return $query_results;	
	}

	public function UpdateMapping($DbName, $PeerStream, $YourStream)
	{
		$db = new SQLite3($DbName);
		$update_query = "UPDATE streams SET STREAM_MAP='" . $YourStream . "' WHERE ID='" . $PeerStream . "'";
		//echo $update_query;
		$db->exec($update_query);	
		$db->close();			
	}

	public function AllCategories($DbName)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT * FROM stream_category";
		$query_results = $db->query($query);
	
		if (!$query_results) die("Cannot execute query.");
	
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
	
		$db->close();
		return $results;		
	}

	public function ViewCategory($DbName, $CategoryID)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT * FROM stream_category where ID = '" . $CategoryID . "'";
		$query_results = $db->query($query);
		if (!$query_results) die("Cannot execute query.");		

		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
		
		$db->close();
		//var_dump($results);
		return $results;		
	}

	public function DeleteCategory($DbName, $CategoryID)
	{
		$db = new SQLite3($DbName);
		$query = "DELETE FROM stream_category WHERE ID ='" . $CategoryID . "'";
		$db->exec($query);
		$db->close();
	}

	public function UpdateCategory($DbName, $CategoryID, $CategoryName, $CategoryGroup)
	{
		$db = new SQLite3($DbName);
		$update_query = "UPDATE stream_category SET 'TEXT'='" . $CategoryName . "', 'Groups'='" . $CategoryGroup . "' WHERE ID=" . $CategoryID;
		echo "<br>";
		echo $update_query ;
		$db->exec($update_query);	
		$db->close();		
	}

	public function InsertCategory($DbName, $CategoryName, $CategoryGroup)
	{
		$db = new SQLite3($DbName);
		$insert_query = 'INSERT INTO stream_category ("TEXT", "Groups") VALUES ("' . $CategoryName . '", "' . $CategoryGroup . '")';
		$db->exec($insert_query);	
		$db->close();		
	}	

	public function UpdateStreamCategory($DbName, $StreamID, $CategoryID)
	{
		$db = new SQLite3($DbName);
		$update_query = "UPDATE streams SET 'STREAM_CAT'='" . $CategoryID . "' WHERE ID='" . $StreamID . "'";
		$db->exec($update_query);	
		$db->close();		
	}	

	public function UpdateCatCounterByStatus($DbName, $CategoryID, $Count, $type)
	{
		$db = new SQLite3($DbName);
		if ($type == 'ALL')
		{
			$update_query = "UPDATE stream_category SET 'STREAMS'='" . $Count . "' WHERE ID='" . $CategoryID . "'";
		}
		elseif ($type == 'ON')
		{
			$update_query = "UPDATE stream_category SET 'STR_Online'='" . $Count . "' WHERE ID='" . $CategoryID . "'";
		}
		elseif  ($type == 'OFF')
		{

		}
		elseif  ($type == 'ACT')
		{
			$update_query = "UPDATE stream_category SET 'STR_Running'='" . $Count . "' WHERE ID='" . $CategoryID . "'";
		}	
		elseif  ($type == 'RUN')
		{
			$update_query = "UPDATE stream_category SET 'STR_Running'='" . $Count . "' WHERE ID='" . $CategoryID . "'";
		}					
		

		$db->exec($update_query);	
		$db->close();		
	}	

	public function DeletePeerStreams($DbName, $PeerID)
	{
		$db = new SQLite3($DbName);
		$query = "DELETE FROM streams WHERE PEER_ID ='" . $PeerID . "'";
		$db->exec($query);
		$db->close();
	}	

	public function DeleteStream($DbName, $StreamID)
	{
		$db = new SQLite3($DbName);
		$query = "DELETE FROM streams WHERE ID ='" . $StreamID . "'";
		$db->exec($query);
		$db->close();
	}		

	public function GetPeerID($DbName, $StreamID)
	{

		$db = new SQLite3($DbName);
		$query = "SELECT PEER_ID FROM streams WHERE ID='" .$StreamID . "'" ;
		$query_results = $db->querySingle($query);	
		$db->close();	

		return $query_results;	
	}

	public function GetStreamNbr($DbName, $StreamID)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT STREAM_NUMBER FROM streams WHERE ID='" .$StreamID . "'" ;
		$query_results = $db->querySingle($query);	
		$db->close();	

		return $query_results;	
	}	

	public static function GetAllStreamsID($DbName)
	{
	
		$db = new SQLite3($DbName);
		//$query = "SELECT ID FROM streams WHERE STREAM_IGN IS NULL AND stream_map IS NOT NULL";
		$query = "SELECT ID FROM streams WHERE STREAM_IGN IS NULL";
		$query_results = $db->query($query);
	
		if (!$query_results) die("Cannot execute query.");
	
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
	
		$db->close();
		return $results;
	}

	public static function GetAllStreamsIDbyCat($DbName, $Cat)
	{
	
		$db 	= new SQLite3($DbName);
		$query 	= "SELECT ID FROM streams WHERE STREAM_IGN IS NULL 
		                                    AND PEER_ID = 0
											AND stream_cat ='" .$Cat . "'";

		//echo $query;
		$query_results = $db->query($query);
	
		if (!$query_results) die("Cannot execute query in GetAllStreamsIDbyCat");
	
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
	
		$db->close();
		return $results;
	}	
	public static function GetNbrStreamsByCatByStatus($DbName, $CatID, $Status)
	{
		$db 	= new SQLite3($DbName);

		if ($Status == 'ALL')
		{
			$query = "SELECT ID FROM streams WHERE STREAM_CAT = '$CatID' ";
//			$query 	= "SELECT STREAMS FROM stream_category WHERE ID ='" . $CatID ."'";
		}
		elseif ($Status == 'ON')
		{
			$query = "SELECT ID FROM streams WHERE STREAM_CAT = '$CatID' AND STREAM_STATUS ='$Status' ";
			//$query 	= "SELECT STR_Online FROM stream_category WHERE ID ='" . $CatID ."'";
		}
		elseif ($Status == 'RUN')
		{
			$query = "SELECT ID FROM streams WHERE STREAM_CAT = '$CatID' AND STREAM_STATUS ='$Status' ";
			//$query 	= "SELECT STR_Running FROM stream_category WHERE ID ='" . $CatID ."'";
		}
		elseif ($Status == 'ACT')
		{
			$query = "SELECT ID FROM streams WHERE STREAM_CAT = '$CatID' AND STREAM_STATUS ='$Status' ";
			//$query 	= "SELECT STR_Running FROM stream_category WHERE ID ='" . $CatID ."'";
		}		
				
	// 	$query = "SELECT ID FROM streams WHERE AND STREAM_STATUS ='ACT' ";

	 	//echo $query;
	 	$query_results = $db->query($query);
	
	// 	if (!$query_results) die("Cannot execute query.");
	
	 	$results = array();
	 	while($data = $query_results->fetchArray())
	 	{
	 		$results[] = $data;
	 	}
	
	 	$db->close();
	 	return $results;
	}	

	public static function GetNbrStreamsOnCatByStatus($DbName, $CatID, $Status)
	{
		$db 	= new SQLite3($DbName);

		if ($Status == 'ALL')
		{
			$query 	= "SELECT STREAMS FROM stream_category WHERE ID ='" . $CatID ."'";
		}
		elseif ($Status == 'ON')
		{
			$query 	= "SELECT STR_Online FROM stream_category WHERE ID ='" . $CatID ."'";
		}
		elseif ($Status == 'RUN')
		{
			$query 	= "SELECT STR_Running FROM stream_category WHERE ID ='" . $CatID ."'";
		}
		elseif ($Status == 'ACT')
		{
			$query 	= "SELECT STR_Running FROM stream_category WHERE ID ='" . $CatID ."'";
		}		
		
		//echo $query;
		$query_results = $db->querySingle($query);
		//if (!$query_results) die("Cannot execute query in GetNbrStreamsOnCatByStatus");
		$db->close();

		return $query_results;		
	}

	public static function GetNbrStreamsByStatus($DbName, $Status)
	{
		$db 	= new SQLite3($DbName);
		$query 	= "SELECT ID FROM streams WHERE STREAM_STATUS ='$Status' ";

		//echo $query;
		$query_results = $db->query($query);
	
		if (!$query_results) die("Cannot execute query.");
	
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
	
		$db->close();
		return $results;		
	}

	// public static function GetNbrStreamsOnline($DbName)
	// {
	// 	$db = new SQLite3($DbName);
	// 	$query = "SELECT ID FROM streams WHERE STREAM_STATUS ='ON' ";

	// 	//echo $query;
	// 	$query_results = $db->query($query);
	
	// 	if (!$query_results) die("Cannot execute query.");
	
	// 	$results = array();
	// 	while($data = $query_results->fetchArray())
	// 	{
	// 		$results[] = $data;
	// 	}
	
	// 	$db->close();
	// 	return $results;		
	// }

	// public static function GetNbrStreamsRunning($DbName)
	// {
	// 	$db = new SQLite3($DbName);
	// 	$query = "SELECT ID FROM streams WHERE AND STREAM_STATUS ='ACT' ";

	// 	//echo $query;
	// 	$query_results = $db->query($query);
	
	// 	if (!$query_results) die("Cannot execute query.");
	
	// 	$results = array();
	// 	while($data = $query_results->fetchArray())
	// 	{
	// 		$results[] = $data;
	// 	}
	
	// 	$db->close();
	// 	return $results;		
	// }

	public static function GetAllStreamsMapped($DbName, $StreamID)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT * FROM streams WHERE STREAM_MAP ='" . $StreamID . "'";

		//echo $query;
		$query_results = $db->query($query);
	
		if (!$query_results) die("Cannot execute query.");
	
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
	
		$db->close();
		return $results;		
	}
	public static function GetAllStreamsOnlineMapped($DbName, $StreamID)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT * FROM streams WHERE STREAM_MAP ='" . $StreamID . "'" . "AND STREAM_STATUS ='ON' ";

		//echo $query;
		$query_results = $db->query($query);
	
		if (!$query_results) die("Cannot execute query.");
	
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
	
		$db->close();
		return $results;		
	}	

	public function InsertNewStream($DbName, $StreamName, $StreamCategory)
	{
		$db = new SQLite3($DbName);
		$PEER_ID = "0";
		$insert_query = 'INSERT INTO streams (STREAM_NAME, PEER_ID, STREAM_CAT) VALUES ("' . $StreamName . '","'. $PEER_ID .'", "'. $StreamCategory .'")';
		$db->exec($insert_query);	
		$LastInsert = $db->lastInsertRowID();
	
		$db->close();	

		return $LastInsert;	
	}
	public function InsertStreamSource($DbName, $StreamName, $StreamCategory, $StreamSource, $StreamMap, $Peer)
	{
		$db = new SQLite3($DbName);
		$PEER_ID = $Peer;
		$insert_query = 'INSERT INTO streams (STREAM_NAME, PEER_ID, STREAM_CAT, SOURCE, STREAM_MAP) VALUES ("' . $StreamName . '","'. $PEER_ID .'", "'. $StreamCategory .'", "'. $StreamSource .'", "'. $StreamMap .'")';
		$db->exec($insert_query);	
		$db->close();	
	}	
	public function UpdateStreamPID($DbName, $StreamID, $StreamPID)
	{
		$db = new SQLite3($DbName);
		$update_query = "UPDATE streams SET 'STREAM_PID'='" . $StreamPID . "' WHERE ID='" . $StreamID . "'";
		$db->exec($update_query);	
		$db->close();		
	}
}