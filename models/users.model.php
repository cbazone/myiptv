<?php

class Users
{

	public $user_id;
	public $user_name;
	public $user_pwd;
	public $user_status;

	public function __construct($user_id, $user_name, $user_pwd, $user_status) 
	{
		$this->user_id     	= $user_id;
		$this->user_name   	= $user_name;
		$this->user_pwd 	= $user_pwd;
		$this->user_status 	= $user_status;
	}

	public static function GetSuperUsers($DbName)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT * FROM users where Type != 'N' ";
		$query_results = $db->query($query);
		
		if (!$query_results) die("Cannot execute query.");
		
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
		
		$db->close();
		return $results;		
	}

	public static function GetUsers($DbName)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT * FROM users where Type = 'N' ";
		$query_results = $db->query($query);
		
		if (!$query_results) die("Cannot execute query.");
		
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
		
		$db->close();
		return $results;		
	}

	public static function GetActiveUsers($DbName)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT * FROM users where Type = 'N' and STATUS = '1'";
		$query_results = $db->query($query);
		
		if (!$query_results) die("Cannot execute query.");
		
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
		
		$db->close();
		return $results;		
	}

	public static function GetUser($DbName, $UserID)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT * FROM users where ID = '". $UserID ."' ";
		$query_results = $db->query($query);
		
		if (!$query_results) die("Cannot execute query.");
		
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
		
		$db->close();
		return $results;		
	}	

	public static function GetUserID($DbName, $UserName)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT ID FROM users where User = '". $UserName ."' ";
		$query_results = $db->querySingle($query);	
		$db->close();	

		return $query_results;		
	}	

	public static function GetUserStatus($DbName, $UserID)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT Status FROM users where ID = '". $UserID ."' ";
		$query_results = $db->querySingle($query);	
		$db->close();	

		return $query_results;		
	}			

	public static function GetUserCategory($DbName, $UserID)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT Category FROM users where ID = '". $UserID ."' ";
		$query_results = $db->querySingle($query);	
		$db->close();	

		return $query_results;			
	}	

	public static function GetUserName($DbName, $UserID)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT User FROM users where ID = '". $UserID ."' ";
		$query_results = $db->querySingle($query);	
		$db->close();	

		return $query_results;			
	}	

	public static function GetUserPassword($DbName, $UserID)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT Password FROM users where ID = '". $UserID ."' ";
		$query_results = $db->querySingle($query);	
		$db->close();	

		return $query_results;			
	}				

	public static function CreateUser($DbName, $UserName, $UserPassword, $UserType, $UserStatus, $UserHost, $UserMaxConnection , $UserExpireDate)
	{
		$db = new SQLite3($DbName);
		$query = "INSERT INTO users (User, Password, Type, Status, Host, MaxConnection, ExpireDate) VALUES ('" . $UserName . "','" . $UserPassword . "','" . $UserType . "','" . $UserStatus ."','" . $UserHost ."','" . $UserMaxConnection ."','" . $UserExpireDate . "')";
		
		$db->exec($query);
		$db->close();		
	}

	public static function DeleteUser($DbName, $UserID )
	{
		$db = new SQLite3($DbName);
		$query = "DELETE FROM users WHERE ID ='" . $UserID . "'";
		$db->exec($query);

		$db->close();		
	}

	public static function Authentification($DbName, $UserName, $Type)	
	{
		$db = new SQLite3($DbName);
		$query = 'SELECT ID,User,Password FROM users WHERE User="'. $UserName .'" AND Type = "' . $Type . '" ' ;

		$query_results = $db->query($query);

		if (!$query_results) die("Cannot execute query.");
		
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
		
		$db->close();
		return $results;		
	}

	public static function SetConnection($DbName, $UserID, $Time)
	{
		$db = new SQLite3($DbName);
		$query =' UPDATE users SET LastConnection="' . $Time . '" WHERE ID="'. $UserID . '"';
		$db->exec($query);

		$db->close();
	}
	public static function SetNumbConnections($DbName, $UserID, $NbConn)
	{
		$db = new SQLite3($DbName);
		$query =' UPDATE users SET ActiveConnection="' . $NbConn . '" WHERE ID="'. $UserID . '"';
		$db->exec($query);

		$db->close();
	}

	public static function SetUsername($DbName, $UserID, $UserName)
	{
		$db = new SQLite3($DbName);
		$query =' UPDATE users SET User="' . $UserName . '" WHERE ID="'. $UserID . '"';
		$db->exec($query);

		$db->close();
	}
	public static function SetUserPassword($DbName, $UserID, $UserPassword)
	{
		$db = new SQLite3($DbName);
		$query =' UPDATE users SET Password="' . $UserPassword . '" WHERE ID="'. $UserID . '"';
		$db->exec($query);

		$db->close();
	}	
	public static function SetUserStatus($DbName, $UserID, $UserStatus)
	{
		$db = new SQLite3($DbName);
		$query =' UPDATE users SET Status="' . $UserStatus . '" WHERE ID="'. $UserID . '"';
		$db->exec($query);

		$db->close();
	}	
	public static function SetUserType($DbName, $UserID, $UserType)
	{
		$db = new SQLite3($DbName);
		$query =' UPDATE users SET Type="' . $UserType . '" WHERE ID="'. $UserID . '"';
		$db->exec($query);

		$db->close();
	}
	public static function SetUserHost($DbName, $UserID, $UserHost)
	{
		$db = new SQLite3($DbName);
		$query =' UPDATE users SET HOST="' . $UserHost . '" WHERE ID="'. $UserID . '"';
		$db->exec($query);

		$db->close();
	}
	public static function SetUserMaxConn($DbName, $UserID, $UserMaxConnection)
	{
		$db = new SQLite3($DbName);
		$query =' UPDATE users SET MaxConnection="' . $UserMaxConnection . '" WHERE ID="'. $UserID . '"';
		$db->exec($query);

		$db->close();
	}
	public static function SetUserExpireDate($DbName, $UserID, $UserExpireDate)
	{
		$db = new SQLite3($DbName);
		$query =' UPDATE users SET ExpireDate="' . $UserExpireDate . '" WHERE ID="'. $UserID . '"';
		$db->exec($query);

		$db->close();
	}
	public static function SetUserCategory($DbName, $UserID, $UserCategory)
	{
		$db = new SQLite3($DbName);
		$query =' UPDATE users SET Category="' . $UserCategory . '" WHERE ID="'. $UserID . '"';
		$db->exec($query);

		$db->close();
	}	
	public static function SetUserUpdatedOn($DbName, $UserID, $Time)
	{
		$db = new SQLite3($DbName);
		$query =' UPDATE users SET UpdatedOn="' . $Time . '" WHERE ID="'. $UserID . '"';
		$db->exec($query);

		$db->close();
	}	
	public static function SetUserUpdatedBy($DbName, $UserID, $ConnectedUser)
	{
		$db = new SQLite3($DbName);
		$query =' UPDATE users SET UpdatedBy="' . $ConnectedUser . '" WHERE ID="'. $UserID . '"';
		$db->exec($query);

		$db->close();
	}
	public static function SetUserCreatedOn($DbName, $UserID, $Time)
	{
		$db = new SQLite3($DbName);
		$query =' UPDATE users SET CreatedOn="' . $Time . '" WHERE ID="'. $UserID . '"';
		$db->exec($query);

		$db->close();
	}	
	public static function SetUserCreatedBy($DbName, $UserID, $ConnectedUser)
	{
		$db = new SQLite3($DbName);
		$query =' UPDATE users SET CreatedBy="' . $ConnectedUser . '" WHERE ID="'. $UserID . '"';
		$db->exec($query);

		$db->close();
	}		
	
	public static function GetHosts($DbName, $type)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT Host FROM users where Type = '$type' ";
		$query_results = $db->query($query);
		
		if (!$query_results) die("Cannot execute query.");
		
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
		
		$db->close();
		return $results;		
	}		
}



