<?php

class Peers
{
	
	public $peer_id;
	public $peer_name;
	public $peer_url;
	public $peer_max;
	

	public function __construct($peer_id, $peer_name, $peer_url, $peer_max) 
	{
		$this->peer_id     	= $peer_id;
		$this->peer_name   	= $peer_name;
		$this->peer_url 	= $peer_url;
		$this->peer_max 	= $peer_max;
	}
	

	public static function AllPeers($DbName)
	{

		$db = new SQLite3($DbName);
		$query = "SELECT * FROM peers where ID > '0' ";
		$query_results = $db->query($query);
		
		if (!$query_results) die("Cannot execute query.");
		
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
		
		$db->close();
		return $results;
	}

	public static function OnePeer($DbName, $PeerID)
	{

		$db = new SQLite3($DbName);
		$query = "SELECT * FROM peers where ID = '" . $PeerID . "'";
		$query_results = $db->query($query);
		if (!$query_results) die("Cannot execute query.");		

		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}
		
		$db->close();
		return $results;	
	}	
	
	public static function Name($DbName, $PeerID)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT PEER_NAME FROM peers WHERE ID='" .$PeerID . "'" ;
		$query_results = $db->querySingle($query);
		
		if (!$query_results) die("Cannot execute query.");
		
		$db->close();
		return $query_results;
	}

	public static function GetPeerURL($DbName, $PeerID)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT PEER_URL FROM peers WHERE ID='" .$PeerID . "'" ;
		$query_results = $db->querySingle($query);
		
		if (!$query_results) die("Cannot execute query.");
		
		$db->close();
		return $query_results;
	}


	public static function Dwl_url($DbName, $PeerID)
	{
		$db = new SQLite3($DbName);
		$query = "SELECT PEER_DWL_URL FROM peers WHERE ID='" .$PeerID . "'" ;
		//echo $query ."<br>";
		$query_results = $db->querySingle($query);
	
		if (!$query_results) die("Cannot execute query.");
	
		$db->close();
		return $query_results;
	}
	
	public static function UpdateCnt($DbName, $PeerID, $TotStreams = 0, $IgnStreams = 0, $MapStreams = 0)
	{
		
		// if ($TotStreams)
		// {
			$db = new SQLite3($DbName);
			$query = "UPDATE peers SET PEER_TOT_STREAMS ='". $TotStreams ."' WHERE ID='".$PeerID . "'";
			//echo $query . "<br>";
			$db->exec($query);
			$db->close();
		// }

		// if ($UseStreams)
		// {
		// 	$query = "UPDATE peers SET PEER_USE_STREAMS ='". $UseStreams ."'";
		// }

		// if ($MapStreams)
		// {
			$db = new SQLite3($DbName);
			$query = "UPDATE peers SET PEER_MAP_STREAMS ='". $MapStreams ."' WHERE ID='".$PeerID . "'";
			//echo $query . "<br>";
			$db->exec($query);
			$db->close();
		// }

		// if ($IgnStreams)
		// {
			$db = new SQLite3($DbName);
			$query = "UPDATE peers SET PEER_IGN_STREAMS ='". $IgnStreams ."' WHERE ID='".$PeerID . "'";
			//echo $query . "<br>";
			$db->exec($query);
			$db->close();
		// }

	}

	public static function UpdatePeer($DbName, $PeerID, $PeerName, $PeerUrl, $PeerMaxConn, $PeerDwlUrl)
	{
		$db = new SQLite3($DbName);
		$query = "UPDATE peers SET PEER_NAME ='". $PeerName ."', PEER_URL = '" .$PeerUrl . "', PEER_MAX_CON = '" . $PeerMaxConn ."', PEER_DWL_URL = '" . $PeerDwlUrl . "'  WHERE ID='".$PeerID . "'";
		$db->exec($query);
		$db->close();		
	}

	public static function AddPeer ($DbName, $PeerName, $PeerUrl, $PeerMaxConn, $PeerDwlUrl, $PeerMail, $PeerSkype)
	{
		$db = new SQLite3($DbName);
		$query = "INSERT INTO peers (PEER_NAME, PEER_URL, PEER_MAX_CON, PEER_DWL_URL, PEER_MAIL, PEER_SKYPE) VALUES ('" . $PeerName . "','" . $PeerUrl . "','" . $PeerMaxConn ."','" . $PeerDwlUrl ."','" . $PeerMail ."','" . $PeerSkype . "')";
		$db->exec($query);

		$PeerID = $db->querySingle("SELECT ID FROM peers WHERE PEER_NAME='" .$PeerName . "'" );
		$db->close();		
		return $PeerID;
	}

	public static function DeletePeer ($DbName, $PeerID )
	{
		$db = new SQLite3($DbName);
		$query = "DELETE FROM peers WHERE ID ='" . $PeerID . "'";
		$db->exec($query);

		$db->close();		
	}	
	
	public static function Update($DbName, $PeerID, $TotStreams, $UseStreams, $IgnStreams, $MapStreams, $LoadTime, $DwlTime, $Mode)
	{
		$db = new SQLite3($DbName);

		if ($Mode == 'load')
		{
				$query = "UPDATE peers SET PEER_TOT_STREAMS ='". $TotStreams ."' , PEER_USE_STREAMS ='" . $UseStreams ."' , PEER_MAP_STREAMS ='" . $MapStreams ."' , PEER_IGN_STREAMS ='" . $IgnStreams ."' , LAST_LIST_LOAD ='" . $LoadTime ."' WHERE ID='".$PeerID . "'";
		}
		elseif ($Mode == 'dwl')
		{
			$query = "UPDATE peers SET LAST_LIST_DWL ='". $DwlTime ."'  WHERE ID='".$PeerID . "'";
		}
		// elseif  ($Mode == 'all')
		// {
		// 	$query = "INSERT into streams (STREAM_NAME, STREAM_NUMBER, PEER_ID) values ('" . $Stream_Name . "' , '" . $Stream_Number . "' , '" . $PeerID . "' )";
		// }
		//echo $query;
		$db->exec($query);
		$db->close();
		
	}
	
	public static function streams($DbName, $PeerID, $StreamType)
	{
		$db = new SQLite3($DbName);

		if ($StreamType == 'all')
		{
			$sel_query = "SELECT * FROM streams WHERE PEER_ID='" .$PeerID . "'" ;
		}
		elseif ($StreamType == 'ign')
		{
			$sel_query = "SELECT * FROM streams WHERE PEER_ID='" . $PeerID . "' AND ( STREAM_IGN is not NULL AND STREAM_IGN is not '' ) ";
		}
		elseif ($StreamType == 'map')
		{
			$sel_query = "SELECT * FROM streams WHERE PEER_ID='" . $PeerID . "' AND ( STREAM_MAP is not NULL AND STREAM_MAP is not '' ) ";
		}	
		//echo $sel_query . "<br>";
		
		$query_results = $db->query($sel_query);
		
		if (!$query_results) die("Cannot execute query.");
		
		$results = array();
		while($data = $query_results->fetchArray())
		{
			$results[] = $data;
		}		
		
		$db->close();
		return $results;		
		
	}
	
	
}