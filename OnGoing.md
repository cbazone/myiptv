On Going..
**********

### Under-Construction ###
* Enable Firewall
* Flash Messages after main actions (connect, update users, peers, ..)
* ReStream using FFmpeg instead of vlc

### Known issues ###
* Creation of Administrator users (incorrect user type, cannot logon)
* Edit user category access right, the drop down doesn't get the current category access