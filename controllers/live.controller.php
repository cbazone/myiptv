<?php 
//include_once($models_path);

class LiveController extends Controller
{
// <HOST>:<PORT>/live/<UserName>/<UserPwd>/<StreamID>.ts

	public function index()
	{
		
		$db_path 	= $this->GetDbPath();
		$UserName 	= $this->GetUsername();
		$UserPWD 	= $this->GetUserPWD();
		$StreamID 	= $this->GetStreamID();

		//echo $UserName . "<br>"; echo $UserPWD . "<br>"; echo $StreamID . "<br>";

		//Set Default to False
		$IsValidUser  = False;
		$IsUserPwdOK  = False;
		$IsUserActive = False;
		$IsCatAccess  = False;		

		$UserID_db = $this->CheckExistingUser($db_path, $UserName);
		if ($UserID_db)
		{
			$IsValidUser  = True;
		}
		else
		{
			$IsValidUser  = False;
			echo "No corresponding user in db";
		}

		$UserPWD_db = $this->CheckUserPwd($db_path, $UserID_db);
		//echo $UserPWD_db . "<br>"; echo $UserPWD . "<br>";
		if ($UserPWD === $UserPWD_db)
		{
			$IsUserPwdOK  = True;
		}
		else
		{
			$IsUserPwdOK  = False;
			echo "Incorrect pwd";
		}		

		$StreamCat_db 		= $this->GetStreamCat($db_path, $StreamID);
		$AllowedCategory 	= $this->GetUserCatAccess($db_path, $UserID_db);
		//echo $StreamCat_db . "<br>"; echo $AllowedCategory . "<br>";
		$AllowedCat 		= $this->AllowedCat($StreamCat_db, $AllowedCategory);

		if ($AllowedCat)
		{
			$IsCatAccess  = True;	
		}
		else
		{
			$IsCatAccess  = False;
			echo "No access to this Category";
		}

		$UserStatus_db	= $this->GetUserStatus($db_path, $UserID_db);

		if ($UserStatus_db == 1)
		{
			$IsUserActive  = True;	
		}
		else
		{
			$IsUserActive  = False;
			echo "User is not active";
		}		

		// echo $IsValidUser . "<br>"; echo $IsUserPwdOK . "<br>"; echo $IsUserActive . "<br>"; echo $IsCatAccess . "<br>";

		if ($IsValidUser AND $IsUserPwdOK AND $IsUserActive AND $IsCatAccess)
		{
			$context = stream_context_create ( array (
				'http' => array (
						'timeout' => 5.0 
				) 
			) );

			$bytes = 0;
			//$url = Config::get('stream_svr_url') . ":" . "2228";
			$port = Config::get('stream_1st_port') + $StreamID;
			

			$engine = $this->CheckEngine();

			if ($engine)
			{
				if ($engine === "vlc")
				{
					$url = "http://127.0.0.1" . ":" . $port;
				} elseif ($engine === "ffmpeg")
				{
					$url = Config::get('tmpfile') . $port . "_.m3u8";
				}

				//echo $url . "<br>";
				$file_handler = fopen ( $url, "rb", false, $context ) or exit ( "Stream Not Working" );
				// Update user last connection
				$Date = date ( 'Y-m-d H:i:s');     																				
		      	Users::SetConnection($db_path, $UserID_db, $Date);	

		      	$this->UserGetNumberConnections($UserID_db);			

				while ( ! feof ( $file_handler )  ) 
				{
					$response = stream_get_line ( $file_handler, 8192 );
					
					$bytes += strlen ( $response );
					echo $response;
				}
				@fclose ( $file_handler );

			} else
			{

			}


		}

	}

	//GetUserName
	public function GetUsername()
	{
		$uri= $_SERVER['REQUEST_URI'];
 		$uri = str_replace('/myiptv/', '', $uri);
		$this->uri = urldecode(trim($uri, '/'));
		$uri_parts = explode('/', $this->uri);
		$UserName = $uri_parts[1];

		return $UserName;
	}

	//GetUserPWD
	public function GetUserPWD()
	{
		$UserPWD 	= $this->Params[0];
		return $UserPWD;
	}	

	public function GetStreamID()
	{
		$StreamID 	= $this->Params[1];
		$StreamID   = str_replace('.ts', '', $StreamID);
		$StreamID   = $StreamID - Config::get('stream_1st_port');
		return $StreamID;
	}	

	//Get UserID
	public function CheckExistingUser($db_path, $UserName)
	{
		$UserID_db = users::GetUserID($db_path, $UserName);


		return $UserID_db;
	}

	//Get UserPwd
	public function CheckUserPwd($db_path, $UserID)
	{
		$UserPWD_db = users::GetUserPassword($db_path, $UserID);

		return $UserPWD_db;
	}	

	//Get UserStatus
	public function GetUserStatus($db_path, $UserID)
	{
		$UserStatus_db = users::GetUserStatus($db_path, $UserID);

		return $UserStatus_db;
	}		

	//Get StreamCategory
	public function GetStreamCat($db_path, $StreamID)
	{
		$StreamCat_db = streams::GetCategory($db_path, $StreamID);

		return $StreamCat_db;
	}	

	public function AllowedCat($Cat, $AllowedCategory)
	{
		$SearchCatString = "-" . $Cat . "-" ;
		if (strpos($AllowedCategory, $SearchCatString) !== false) 
		{
    		$AllowedCat = 1;
		}
		else
		{
			$AllowedCat = 0;
		}

		return $AllowedCat;
	}

	public function GetUserCatAccess($db_path, $UserID)
	{
		$AllowedCategory = Users::GetUserCategory($db_path, $UserID);
		return $AllowedCategory;
	}		
}
