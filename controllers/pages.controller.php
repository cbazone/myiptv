<?php

class PagesController extends Controller{
	
	public function index()
	{

		$this->Data['OS'] 		= $this->GetServerOS();
		$this->Data['KERNEL'] 	= $this->GetServerKernel();
		$this->Data['IP'] 		= $this->GetServerIP();
		$this->Data['LoadAverage'] 		= $this->GetServerCpuUsage();
		$this->Data['MemoryUsage'] 		= $this->GetServerMemoryUsage();
		$RX = $this->GetNetwork(Config::get('net_interface'), "rx_bytes");
		$TX = $this->GetNetwork(Config::get('net_interface'), "tx_bytes");	
		$this->Data['RX'] 		= $this->ConvertByte($RX);
		$this->Data['TX'] 		= $this->ConvertByte($TX);
		$this->Data['UT'] 		= $this->GetUpTime();

		$this->Data['PHP_VER'] 			= $this->GetPHPVersion();
		$this->Data['APACHE_VER'] 		= $this->GetApacheVersion();
		$this->Data['NGINX_VER'] 		= $this->GetNginxVersion();
		$this->Data['VLC_VER'] 			= $this->GetVLCVersion();
		$this->Data['PHPFPM_VER'] 		= $this->GetPhpFPMVersion();
		$this->Data['FFMPEG_VER'] 		= $this->GetFFmpegVersion();

		$this->Data['IPTABLES_VER'] 	= $this->GetIPtablesVersion();
		$this->Data['UFW_VER'] 			= $this->GetUFWVersion();	

		$this->Data['Online_clients'] 	= $this->UserGetNumberConnected();
		$this->Data['Active_clients'] 	= $this->GetActiveClients();
		$this->Data['Total_clients'] 	= $this->GetTotalClients();

		$this->Data['Online_ServerStreams'] 	= $this->GetStreams("server", "online");
		$this->Data['Active_ServerStreams'] 	= $this->GetStreams("server", "active");
		$this->Data['Total_ServerStreams'] 		= $this->GetStreams("server", "total");

		$this->Data['Online_PeerStreams'] 		= $this->GetStreams("peer", "online");
		$this->Data['Map_PeerStreams'] 			= $this->GetStreams("peer", "MAP");
		$this->Data['Ign_PeerStreams'] 			= $this->GetStreams("peer", "IGN");
		$this->Data['Total_PeerStreams'] 		= $this->GetStreams("peer", "total");

	}


	public function GetStreams($Owner, $Status)
	{
		$db_path 		= $this->GetDbPath();

		if ($Owner == "server")
		{
			if ($Status == "total")
			{
				$streams 	= count(STREAMS::YourStream($db_path));
			}
			elseif ($Status == "online")
			{
				$streams 	= count(STREAMS::YourStreamStatus($db_path, "ON" ));
			}			
			elseif ($Status == "active")
			{
				$streams 	= count(STREAMS::YourStreamStatus($db_path, "ACTIVE"));
			}
			
		}
		else
		{
			// Owner == Peer
			if ($Status == "total")
			{
				$streams 	= count(STREAMS::PeerStreams($db_path));
			}
			elseif ($Status == "online")
			{
				$streams 	= count(STREAMS::PeerOnlineStreams($db_path));
			}			
			elseif ($Status == "MAP")
			{
				$streams 	= count(STREAMS::PeerMAPStreams($db_path));
			}	
			elseif ($Status == "IGN")
			{
				$streams 	= count(STREAMS::PeerIGNStreams($db_path));
			}						

		}
		
		return $streams;
	}
	
	public function view(){
		$params = App::getRouter()->getParams();
		
		if ( isset($params)){
			$alias = strtolower($params[0]);
			
			//echo "here will be a page with '{$alias}' alias";
			$this->Data['content'] = "here will be a page with '{$alias}' alias";
		}
	}

	public function GetTotalClients()
	{
		$db_path 		= $this->GetDbPath();
		$Total_clients 	= count(USERS::GetUsers($db_path));

		return $Total_clients;
	}

	public function GetActiveClients()
	{
		$db_path 		= $this->GetDbPath();
		$Active_clients = count(USERS::GetActiveUsers($db_path));

		return $Active_clients;
	}	
}