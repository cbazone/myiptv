<?php
include_once($models_path);

class UsersController extends Controller{
	
	public function index()
	{
		$db_path 	= $this->GetDbPath();

		$User 		= Users::GetUsers($db_path);
		$SuperUser 	= Users::GetSuperUsers($db_path);

		foreach ($User as $u) 
		{
			//Update the number of connection
			$this->UserGetNumberConnections($u['ID']);
		}

		$this->Data['SuperUsers'] 	= $SuperUser;
		$this->Data['Users'] 		= $User;

	}

	public function add()
	{
		$db_path 	= $this->GetDbPath();
		$PeerID 	= $this->Params[0];

		if ($PeerID)
		{
			$PeerInfo = Peers::OnePeer($db_path, $PeerID);
			$this->Data['Peer'] = $PeerInfo;
		}

	}

	public function CreateUser()
	{
		global $_SESSION;
		session_start();
		$db_path 	= $this->GetDbPath();

		if(isset($_POST['inputUserName']))
		{
			$UserName 					= $_POST['inputUserName'];
		}
		if(isset($_POST['inputPassWord']))
		{
			$UserPassword 				= $_POST['inputPassWord'];
		}
		if(isset($_POST['inputUserStatus']))
		{
			$UserStatus 				= $_POST['inputUserStatus'];
		}
		if(isset($_POST['inputUserHost']))
		{
			$UserHost 					= $_POST['inputUserHost'];
		}			
		if(isset($_POST['inputUserMaxConnection']))
		{
			$UserMaxConnection 			= $_POST['inputUserMaxConnection'];
		}	
		if(isset($_POST['inputUserExpireDate']))
		{
			$startDate = time ();
			$UserExpireDate 			= date ( 'Y-m-d H:i:s', strtotime ( '+' . $_POST['inputUserExpireDate'] . ' day', $startDate ) );;
		}
		if(isset($_POST['inputUserType']))
		{
			$UserType 					= $_POST['inputUserType'];
		}
		else
		{
			$UserType 					= 'N';
		}

		Users::CreateUser($db_path, $UserName, $UserPassword, $UserType, $UserStatus, $UserHost, $UserMaxConnection , $UserExpireDate);

		$message = array(["S", USER_CREATED]);
		$this->redirect("users", "", array(), $message);
	}	

	public function delete($UserID = 0)
	{
		$db_path 	= $this->GetDbPath();
		$UserID 	= $this->GetUserID($UserID);

		Users::DeleteUser($db_path, $UserID );

		$message = array(["W", USER_DELETED]);
		$this->redirect("users", "", array(), $message);		
	}
	
	public function GetUserID($UserID = 0 )
	{
		if ($UserID)
		{

		} else
		{
			$UserID 	= $this->Params[0];
		}

		return $UserID;
	}	

	public function IsConnect()
	{
		global $_SESSION; 																// La fonction récupère les informations de session.
  		if( isset($_SESSION["id"]) && $_SESSION['id']!=0)  								// l'id est inexistant ou null
  		{ 
    		return true; 																// L'utilisateur n'est pas connecté.
  		}
  			else  																		// l'id existe et n'est pas null
  		{
    		return false; 																// L'utilisateur est connecté.
  		}
	}

	public function Autentification()
	{
		global $_POST; 																	// On charge les données POST.
		$db_path 	= $this->GetDbPath();
		$User = $_POST['inputUserName'];
		$type = "SU";
		//$messages = array();

		$reponse = Users::Authentification($db_path, $User, $type);

    	foreach ($reponse as $db_data) 
    	{
    		$db_pwd 	= $db_data['Password'];
    		$db_id 		= $db_data['ID'];
    		$db_user 	= $db_data['User'];
    	}

  		if(count($reponse) > 0)  														// Le nombre de résultats est supérieur à 0, le pseudonyme est bon
  		{
    		//$resultat = mysql_fetch_assoc($reponse);
    		// echo "Post PWD: " . $_POST['inputUserPassword'] . "<br>";
    		// echo "db PWD: " .$db_pwd . "<br>";
    		if( $_POST['inputUserPassword'] == $db_pwd )
    		{

	    		//if( sha1($_POST['mdp'] == $reponse['Password'] ) )					// Le mot de passé saisi (et crypté) correspond à celui de la base de données
	      			$_SESSION['id'] 	= $db_id;
	      			$_SESSION['User'] 	= $db_user;	
	      			//var_dump($_SESSION['id']);
	      			$Date = date ( 'Y-m-d H:i:s');
	      																				// L'utilisateur est défini comme l'administrateur portant l'id du pseudo
	      			Users::SetConnection($db_path, $db_id, $Date);						// Mise à jour de la dernière connexion
	      			
	      			$message = array(["S", CONNECT_MSG]);	
	      			$this->redirect("", "", array(), $message);						// On relance la page.
	      			return "Vous êtes connecté";
	    	}
	    	else  																		// Mot de passe erroné.
	    	{
				$message = array(["W", CONNECT_ERR1]);	
	      		$this->redirect("", "", array(), $message);	    		
	      		//return "Pseudonyme ou Mot de passe incorrect."; 						// Message d'erreur

	    	}	
  		}
  		else  																			// Le pseudonyme n'a pas été trouvé.
  		{
  			$message = array(["W", CONNECT_ERR2]);	
	      	$this->redirect("", "", array(), $message);	
    		//return "Pseudonyme ou Mot de passe incorrect."; 							// Message d'erreur
  		}
	}

	public function logout()
	{
		session_start();

		// Détruit toutes les variables de session
		$_SESSION = array();

		// Si vous voulez détruire complètement la session, effacez également
		// le cookie de session.
		// Note : cela détruira la session et pas seulement les données de session !
		if (ini_get("session.use_cookies")) {
		    $params = session_get_cookie_params();
		    setcookie(session_name(), '', time() - 42000,
		        $params["path"], $params["domain"],
		        $params["secure"], $params["httponly"]
		    );
		}

		// Finalement, on détruit la session.
		session_destroy();
		$message = array(["I", DISCONNECT_MSG]);
		$this->redirect("", "", array(), $message);
	}

	public function edit()
	{
		$db_path 	= $this->GetDbPath();
		$UserID 	= $this->GetUserID($UserID);	

		$UserInfo = Users::GetUser($db_path, $UserID );
		$this->Data['User'] = $UserInfo;

		$CatInfo = Streams::AllCategories($db_path);
		usort($CatInfo, function($a, $b) 
		{
	        return strcasecmp($a['Groups'], $b['Groups']);
	        //return $a['TEXT'] - $b['TEXT'];
		});

		$this->Data['AllCategory'] = $CatInfo;	

	}

	public function EditUser()
	{
		global $_SESSION;
		session_start();
		$db_path 		= $this->GetDbPath();
		$UserID 		= $this->GetUserID($UserID);
		$ConnectedUser 	= $_SESSION['User'];
		//var_dump($_SESSION);
		$Time 			= date ( 'Y-m-d H:i:s');
		
		if(isset($_POST['inputUserName']))
		{
			$UserName 					= $_POST['inputUserName'];
			Users::SetUsername($db_path, $UserID, $UserName);
		}
		if(isset($_POST['inputPassWord']) &&!empty($_POST['inputPassWord']) )
		{
			$UserPassword 				= $_POST['inputPassWord'];
			Users::SetUserPassword($db_path, $UserID, $UserPassword);
		}
		if(isset($_POST['inputUserStatus']))
		{
			$UserStatus 				= $_POST['inputUserStatus'];
			Users::SetUserStatus($db_path, $UserID, $UserStatus);
		}
		if(isset($_POST['inputUserHost']))
		{
			$UserHost 					= $_POST['inputUserHost'];
			Users::SetUserHost($db_path, $UserID, $UserHost);
		}			
		if(isset($_POST['inputUserMaxConnection']))
		{
			$UserMaxConnection 			= $_POST['inputUserMaxConnection'];
			Users::SetUserMaxConn($db_path, $UserID, $UserMaxConnection);
		}	
		if(isset($_POST['inputUserExpireDate']))
		{
			$startDate = time ();
			$UserExpireDate 			= date ( 'Y-m-d H:i:s', strtotime ( '+' . $_POST['inputUserExpireDate'] . ' day', $startDate ) );
			Users::SetUserExpireDate($db_path, $UserID, $UserExpireDate);
		}
		if(isset($_POST['inputUserType']))
		{
			$UserType 					= $_POST['inputUserType'];
		//	Users::SetUserType($db_path, $UserID, $UserType);
		//}
		//else
		//{
		//	$UserType 					= 'N';
			Users::SetUserType($db_path, $UserID, $UserType);
		}
		if(isset($_POST['InputUserCategory']))
		{
			$UserCategory = "";
			$InputUserCategory = $_POST['InputUserCategory'];

			sort($InputUserCategory);
			//var_dump($InputUserCategory); echo "<br>";

			foreach ($InputUserCategory as $selectedOption)
				$UserCategory = $UserCategory . '-' . $selectedOption;
    		
    		$UserCategory = $UserCategory . '-';
    		//echo $UserCategory."<br>";
    		Users::SetUserCategory($db_path, $UserID, $UserCategory);
		}

		Users::SetUserUpdatedOn($db_path, $UserID, $Time);
		Users::SetUserUpdatedBy($db_path, $UserID, $ConnectedUser);
		$message = array(["I", USER_UPDATED . "[" . $UserName . "]"]);
		$this->redirect("users", "", array(), $message);
	}

	public function deactivate()
	{
		global $_SESSION;
		session_start();
		
		$db_path 		= $this->GetDbPath();
		$UserID 		= $this->GetUserID($UserID);
		$UserName 		= $this->GetUserName($db_path, $UserID);
		$ConnectedUser 	= $_SESSION['User'];
		$Time 			= date ( 'Y-m-d H:i:s');	

		$ActualStatus 	= $this->Params[1];

		if ($ActualStatus == 1)
		{ //Deactivate the User
			$UserStatus = 0;
			$message = array(["W", USER_DEACTIVATED . " [" . $UserName . "]"]);
		}
		else
		{ // Reactivate the User
			$UserStatus = 1;
			$message = array(["S", USER_ACTIVATED . " [" . $UserName . "]"]);
		}

		Users::SetUserStatus($db_path, $UserID, $UserStatus);
		Users::SetUserUpdatedOn($db_path, $UserID, $Time);
		Users::SetUserUpdatedBy($db_path, $UserID, $ConnectedUser);		

		
		$this->redirect("users", "", array(), $message);
	}	

	// public function delete()
	// {
		
	// }

	public function download()
	{
		$db_path 		= $this->GetDbPath();
		$UserID 		= $this->GetUserID($UserID);

		$AllowedCategory 	= $this->GetUserCatAccess($db_path, $UserID);
		$stream_svr_url		= Config::get('stream_svr_url');
		$stream_svr_port	= Config::get('stream_svr_port');
		$UserName 			= $this->GetUserName($db_path, $UserID);
		$UserPassword 		= $this->GetUserPassword($db_path, $UserID);
		$AllCategories 		= Streams::AllCategories($db_path);
		

		usort($AllCategories, function($a, $b) 
		{
	        return strcasecmp($a['TEXT'], $b['TEXT']);
		});

		if ($AllowedCategory)
		{
			header("Content-type: text/html; charset=utf-8");
			header("Content-Disposition: attachment; filename=". $UserName. "_iIPTV-vlc.m3u");
			echo "#EXTM3U\n";
		}

		foreach ($AllCategories as $Cat) 
		{

			$AllowedCat = $this->AllowedCat($Cat['ID'], $AllowedCategory);

			if ($AllowedCat)
			{
				echo "#EXTINF:-1, ### " . $Cat['TEXT'] . " ###\n";
				$YourStreamInCat = Streams::YourStreamInCat($db_path, $Cat['ID']);
				
				foreach ($YourStreamInCat as $Stream)
				{
					echo "#EXTINF:-1, " . $Stream['STREAM_NAME'] . " \n";
					$PortStream = Config::get('stream_1st_port') + $Stream['ID'];
					echo $stream_svr_url . ":" . $stream_svr_port . Config::get('base_url') . "live" . DS . $UserName . DS . $UserPassword . DS . $PortStream . ".ts\n";
				}
			}
		}

		

		//$this->Data['AllowedCategory'] = $AllowedCategory ;	

	}

	public function GetUserCatAccess($db_path, $UserID)
	{
		$AllowedCategory = Users::GetUserCategory($db_path, $UserID);
		return $AllowedCategory;
	}	

	public function GetUserName($db_path, $UserID)
	{
		$UserName = Users::GetUserName($db_path, $UserID);
		return $UserName;
	}

	public function GetUserPassword($db_path, $UserID)
	{
		$UserPassword = Users::GetUserPassword($db_path, $UserID);
		return $UserPassword;

	}		

	public function AllowedCat($Cat, $AllowedCategory)
	{
		$SearchCatString = "-" . $Cat . "-" ;
		if (strpos($AllowedCategory, $SearchCatString) !== false) 
		{
    		$AllowedCat = 1;
		}
		else
		{
			$AllowedCat = 0;
		}

		return $AllowedCat;
	}

}