<?php
include_once($models_path);

class ToolsController extends Controller{

	public function index()
	{

	}
 
	public function GetHostsUser($type)
	{
		$db_path 	= $this->GetDbPath();
		$hosts 		= Users::GetHosts($db_path, $type);

		return $hosts;
	}

	public function RunFWCmd($host, $chain, $port)
	{

		$iptables_cmd 	= Config::get('iptables_cmd');
		$iptables_cmd 	= $iptables_cmd . " -A " . $chain;
		$iptables_cmd 	= $iptables_cmd . " -p tcp -s " . $host . " --dport " . $port;
		echo $iptables_cmd . "<br>";
		shell_exec ( $iptables_cmd );
	}

	public function FWCreateChain($chain)
	{
		$iptables_cmd 	= Config::get('iptables_cmd');
		$iptables_cmd 	= $iptables_cmd . " -N " . $chain;
	}

	public function FWFlushChain($chain)
	{
		$iptables_cmd 	= Config::get('iptables_cmd');
		$iptables_cmd 	= $iptables_cmd . " -F " . $chain;
		echo $iptables_cmd . "<br>";
	}


	public function FWUser()
	{
		$hosts = $this->GetHostsUser('N');
		$port  = Config::get('stream_svr_port');
		$chain = Config::get('echange_chain');

		$this->FWFlushChain($chain);

		foreach ($hosts as $host) 
		{
			if (!empty($host[0]))
			{
				$this->RunFWCmd($host[0], $chain, $port);
			}
		}		
	}

	public function FWSuperUser()
	{
		$hosts = $this->GetHostsUser('SU');
		$port  = Config::get('stream_ech_port');
		$chain = Config::get('panel_chain');

		$this->FWFlushChain($chain);

		foreach ($hosts as $host) 
		{
			if (!empty($host[0]))
			{
				$this->RunFWCmd($host[0], $chain, $port);
			}
		}		
	}

	public function FWStatus()
	{
		$fw_cmd = Config::get('fw_cmd');
		$fw_cmd = $fw_cmd . " status";
		$status = shell_exec ( $iptables_cmd );

		var_dump($status);
	}

}
