<?php
include_once($models_path);

class PeersController extends Controller{
	
	public function index()
	{
		$db_path 	= $this->GetDbPath();
		
		$AllPeers = Peers::AllPeers($db_path);
		$this->Data['AllPeers'] = $AllPeers;		
	}	
	
	public function load($PeerID)
	{
		$db_path 	= $this->GetDbPath();
		
		//$PeerID = $this->Params[0];
		$PeerID 	= $this->GetPeerID($PeerID);
		$PeerName = Peers::Name($db_path, $PeerID);
		

		$file_name = $PeerName . ".m3u";
		$file_path = ROOT.DS.'peer_streams'.DS;
		$file = $file_path . $file_name;
		
		$myfile = fopen($file, "r") or die("Unable to open file!");
		$content = fread($myfile,filesize($file));
		$rows = explode("#EXTINF:-1,", $content);
		array_shift($rows);
		$NumbIgnStreams = 0;
		$NumbMapStreams = 0;
		$NumbUseStreams = 0;
		
		foreach($rows as $row => $data)
		{

			$stream_name = $this->StreamName($data);
			$stream_number = $this->StreamNumber($data);

			//	echo $stream_name . "<br>";
			if ($stream_name[0] <> '#' and  $stream_name[0] <> '@')
			{
				streams::InsertStream($db_path, $PeerID, $stream_name, $stream_number);
			}
			
		}	
		
		$TotStreams = count($rows);
		$LoadTime = date('Y-m-d h:i:s');
		
		$this->Data['#Streams'] 	= $TotStreams;
		$this->Data['#IgnStreams'] 	= $NumbIgnStreams;
		$this->Data['#MapStreams'] 	= $NumbMapStreams;
		$this->Data['LoadTime'] 	= $LoadTime;
		
		peers::Update($db_path, $PeerID, $TotStreams, $NumbUseStreams, $NumbIgnStreams, $NumbMapStreams, $LoadTime, $dwlTime, 'load' );

		$this->UpdateCounters($db_path, $PeerID);
		
		
	}
	
	public function download($PeerID)
	{
		
		global $_SESSION;
		$this->SessionStarted();
		
		$db_path 	= $this->GetDbPath();
		
		//$PeerID = $this->Params[0];
		$PeerID 	= $this->GetPeerID($PeerID);
		$Dwl_url 	= Peers::Dwl_url($db_path, $PeerID);
		$PeerName 	= Peers::Name($db_path, $PeerID);
		$file  		= ROOT.DS.'peer_streams'.DS.$PeerName.".m3u";
		
		$dwlTime 	= date('Y-m-d h:i:s');
		$wget_cmd 	= 'wget ' .'-O ' . $file . ' ' . '"' . $Dwl_url . '"';
		
		$this->Data['Dwl_url'] 		= $Dwl_url;
		$this->Data['dwlTime'] 		= $dwlTime;
		$this->Data['wget_cmd'] 	= $wget_cmd;
		
		//echo $wget_cmd;
		exec($wget_cmd);
		peers::Update($db_path, $PeerID, $TotStreams, $NumbUseStreams, $NumbIgnStreams, $NumbMapStreams, $LoadTime, $dwlTime, 'dwl');

		$messages[] = ["I", PEER_DWL_MSG_001 . "[" . $PeerID ."]"];
		$messages[] = ["I", PEER_DWL_MSG_002 . "[" . $Dwl_url ."]"];
		$messages[] = ["I", PEER_DWL_MSG_003 . "[" . $dwlTime ."]"];
		$messages[] = ["I", PEER_DWL_MSG_004 . "[" . $wget_cmd ."]"];

		$this->redirect("peers", "", array(), $messages);
	}
	
	public function viewallstreams()
	{
		$db_path 	= $this->GetDbPath();
		
		$PeerID = $this->Params[0];
		$StreamType = 'all';

		$StreamList = Peers::streams($db_path, $PeerID, $StreamType);
		$YourStream = Streams::YourStream($db_path);
		
		$this->Data['AllStreams'] 	= $StreamList;
		$this->Data['YourStreams'] 	= $YourStream;

		$this->UpdateCounters($db_path, $PeerID);
		
	}

	public function viewignstreams()
	{
		$db_path 	= $this->GetDbPath();
		
		$PeerID = $this->Params[0];
		$StreamType = 'ign';
		$StreamList = Peers::streams($db_path, $PeerID, $StreamType);
		
		$this->Data['IgnStreams'] 	= $StreamList;

		$this->UpdateCounters($db_path, $PeerID);
		
	}
	public function viewmapstreams()
	{
		$db_path 	= $this->GetDbPath();
		
		$PeerID = $this->Params[0];
		$StreamType = 'map';
		$StreamList = Peers::streams($db_path, $PeerID, $StreamType);
		
		$this->Data['MapStreams'] 	= $StreamList;

		$this->UpdateCounters($db_path, $PeerID);
		
	}	

	public function UpdateCounters($db_path, $PeerID)
	{
		$TotStreams 	= count(Peers::streams($db_path, $PeerID, 'all'));
		//$NumbUseStreams = count(Peers::streams($db_path, $PeerID, 'use'));
		$NumbIgnStreams = count(Peers::streams($db_path, $PeerID, 'ign'));
		$NumbMapStreams = count(Peers::streams($db_path, $PeerID, 'map'));

		peers::UpdateCnt($db_path, $PeerID, $TotStreams, $NumbIgnStreams, $NumbMapStreams);
	}

	public function StreamName($data)
	{
		//Retrieve the StreamName from the file

			//get row data
			$row_data = explode('^', $data);
			$stream_name = explode('http', $row_data[0]);
			$stream_name = $stream_name[0];
			$stream_name = str_replace(array("\n", "\t", "\r"), '', $stream_name);	

			return $stream_name;

	}

	public function StreamNumber($data)
	{
		//Retrieve the StreamNumber from the file

			//get row data
			$row_data = explode('^', $data);
			$stream_url = explode('http', $row_data[0]);
			$stream_url = $stream_url[1];
			// get the last part of the url
			$stream_number = substr($stream_url, strrpos($stream_url, '/') + 1);
			// Remove the .ts
			$stream_number = str_replace(array(".", "ts"), '', $stream_number);

			return $stream_number;
	}	

	public function IgnoreStream()
	{
		$db_path 	= $this->GetDbPath();
		
		$PeerID 	= $this->Params[0];
		$StreamID 	= $this->Params[1];

		streams::IgnoreStream($db_path, $PeerID, $StreamID, 'X');

		$this->UpdateCounters($db_path, $PeerID);

		$_SESSION['MSG']= "Stream " .  $StreamID . "added to the ignoreList";
		$this->redirect("peers", "viewallstreams", array($PeerID));
	}

	public function KeepStream()
	{
		$db_path 	= $this->GetDbPath();
		
		$PeerID 	= $this->Params[0];
		$StreamID 	= $this->Params[1];

		streams::IgnoreStream($db_path, $PeerID, $StreamID, ' ');

		$this->UpdateCounters($db_path, $PeerID);

		$this->redirect("peers", "viewallstreams", array($PeerID));
	}	

	public function CheckStreamStatus($PeerID, $StreamID)
	{
		$db_path 	= $this->GetDbPath();
		$PeerID 	= $this->GetPeerID($PeerID);
		$StreamID 	= $this->GetStreamID($StreamID);

		if ($PeerID)
		{
			if ($StreamID)
			{
				$StreamSrc = streams::StreamSrc($db_path, $StreamID);

				$stream_chk = shell_exec ( "ffprobe -v quiet -print_format json -show_streams '$StreamSrc' " );
			
				if (json_decode($stream_chk, true)){
					$Status = "ON";
				} else {
					$Status = "OFF";
				}

				streams::StreamStatus($db_path, $StreamID, $Status);

			} else
			{
				$StreamType = 'all';
				$StreamList = Peers::streams($db_path, $PeerID, $StreamType);

				foreach($StreamList as $Stream)
				{
					$this->CheckStreamStatus($PeerID, $Stream['ID']);
				
				}


			}
		} else
		{
			$StreamList = streams::all($db_path);
			foreach($StreamList as $Stream)
				{
					$this->CheckStreamStatus($Stream['PEER_ID'], $Stream['ID']);				
				}			
		}

		$this->redirect("peers", "viewallstreams", array($PeerID));
	}

	public function CheckStreamQuality($PeerID, $StreamID)
	{
		$db_path 	= $this->GetDbPath();
		$PeerID 	= $this->GetPeerID($PeerID);
		$StreamID 	= $this->GetStreamID($StreamID);

		//var_dump($PeerID);

		if ($PeerID)
		{
			if ($StreamID)
			{
				$StreamSrc = streams::StreamSrc($db_path, $StreamID);

				$stream_chk = shell_exec ( "ffprobe -v quiet -print_format json -show_streams '$StreamSrc' " );
			
				if (json_decode($stream_chk, true)){
					var_dump($stream_chk);
					//$Stream_VC 		= 'N/A';
					//$Stream_AC 		= 'N/A';
					//$Stream_BitRate = 'N/A';
				} else {
					$Stream_VC 		= 'N/A';
					$Stream_AC 		= 'N/A';
					$Stream_BitRate = 'N/A';
				}

				streams::StreamQuality($db_path, $StreamID, $Stream_VC, $Stream_AC, $Stream_BitRate);

			} else
			{
				$StreamType = 'all';
				$StreamList = Peers::streams($db_path, $PeerID, $StreamType);

				foreach($StreamList as $Stream)
				{
					$this->CheckStreamQuality($PeerID, $Stream['ID']);

				}


			}
		} else
		{
			$StreamList = streams::all($db_path);
			foreach($StreamList as $Stream)
				{
					$this->CheckStreamStatus($Stream['PEER_ID'], $Stream['ID']);				
				}			
		}

		$this->redirect("peers", "viewallstreams", array($PeerID));
	}

	// public function GetDbPath()
	// {
	// 	$db_name 	= 'IPTV.db';
	// 	$db_path 	= ROOT.DS.'db'.DS.$db_name;

	// 	return $db_path;
	// }

	public function GetPeerID($PeerID = 0 )
	{
		if ($PeerID)
		{

		} else
		{
			$PeerID 	= $this->Params[0];
		}

		return $PeerID;
	}	

	public function GetStreamID($StreamID = 0)
	{
		if ($StreamID)
		{

		} else
		{
			$StreamID 	= $this->Params[1];
		}

		return $StreamID;
	}	

	public function view($PeerID = 0 )
	{
		$db_path 	= $this->GetDbPath();
		$PeerID 	= $this->GetPeerID($PeerID);

		$PeerInfo = Peers::OnePeer($db_path, $PeerID);
		$this->Data['PeerInfo'] 	= $PeerInfo;

	}

	public function edit($PeerID = 0 )
	{
		$db_path 	= $this->GetDbPath();
		$PeerID 	= $this->GetPeerID($PeerID);

		$PeerInfo 	= Peers::OnePeer($db_path, $PeerID);
		$this->Data['PeerInfo'] 	= $PeerInfo;

	}

	public function create($PeerID = 0 )
	{
		$db_path 	= $this->GetDbPath();
		//$PeerID 	= $this->GetPeerID($PeerID);

		//$PeerInfo 	= Peers::OnePeer($db_path, $PeerID);
		//$this->Data['PeerInfo'] 	= $PeerInfo;

	}

	public function delete($PeerID = 0 )
	{
		$db_path 	= $this->GetDbPath();
		$PeerID 	= $this->GetPeerID($PeerID);

		Peers::DeletePeer($db_path, $PeerID);
		Streams::DeletePeerStreams($db_path, $PeerID);

		$this->redirect("peers", "", array());

	}	


	public function CreatePeer($PeerID = 0 )
	{
		$db_path 	= $this->GetDbPath();

		if(isset($_POST['inputPeerName']))
		{
			$PeerName 			= $_POST['inputPeerName'];
		}
		if(isset($_POST['inputPeerUrl']))
		{
			$PeerUrl 			= $_POST['inputPeerUrl'];
		}
		if(isset($_POST['inputPeerMaxConnection']))
		{
			$PeerMax 			= $_POST['inputPeerMaxConnection'];
		}
		if(isset($_POST['inputPeerDwlUrl']))
		{
			$PeerDwl 			= $_POST['inputPeerDwlUrl'];
		}			
		if(isset($_POST['inputPeerMail']))
		{
			$PeerMail 			= $_POST['inputPeerMail'];
		}	
		if(isset($_POST['inputPeerSkype']))
		{
			$PeerSkype 			= $_POST['inputPeerSkype'];
		}	

		$PeerID = Peers::AddPeer($db_path, $PeerName, $PeerUrl, $PeerMax, $PeerDwl, $PeerMail, $PeerSkype );

		$this->download($PeerID);
		$this->load($PeerID);
		
		$this->redirect("peers", "edit", array($PeerID));

	}		

	public function EditPeer()
	{
		$db_path 	= $this->GetDbPath();
		$PeerID 	= $this->Params[0];


		if(isset($_POST['inputPeerName']))
		{
			$PeerName 			= $_POST['inputPeerName'];
		}
		if(isset($_POST['inputPeerUrl']))
		{
			$PeerUrl 			= $_POST['inputPeerUrl'];
		}
		if(isset($_POST['inputPeerMaxConnection']))
		{
			$PeerMax 			= $_POST['inputPeerMaxConnection'];
		}
		if(isset($_POST['inputPeerDwlUrl']))
		{
			$PeerDwl 			= $_POST['inputPeerDwlUrl'];
		}	

		Peers::UpdatePeer($db_path, $PeerID, $PeerName, $PeerUrl, $PeerMax, $PeerDwl);

		// echo $PeerName . "<br>";
		// echo $PeerUrl . "<br>";
		// echo $PeerMax . "<br>";
		// echo $PeerDwl. "<br>";

		$this->redirect("peers", "edit", array($PeerID));
	}

	public function UpdateMapping()
	{
		
		$db_path 	= $this->GetDbPath();
		$PeerID         = $this->Params[0];
		$PeerStream 	= $this->Params[1];
		$YourStream 	= $this->Params[2];

		Streams::UpdateMapping($db_path, $PeerStream, $YourStream);

		$this->redirect("peers", "viewallstreams", array($PeerID));

	}

}