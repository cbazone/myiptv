<?php
include_once($models_path);

class StreamsController extends Controller{
	
	
	public function index()
	{

		$db_path 	= $this->GetDbPath();
		$Allstreams = Streams::YourStream($db_path);

		usort($Allstreams, function($a, $b) 
		{
        	return strcasecmp($a['STREAM_NAME'], $b['STREAM_NAME']);
        	//return $a['STREAM_NAME'] - $b['STREAM_NAME'];
		});

		//$StreamBackups = Streams::FindAllBackups($db_path, $StreamID);
		$StreamBackups = Streams::FindAllBackups($db_path);
		$AllCategories = Streams::AllCategories($db_path);

		if (empty($AllCategories))
		{
			$this->CreateDefaultCategory();
		}

		usort($AllCategories, function($a, $b) 
		{
	        return strcasecmp($a['TEXT'], $b['TEXT']);
		});

		foreach ($AllCategories as $Cat) 
		{
			$this->UpdateCatCounter($db_path, $Cat["ID"]);
		}

		// $CountUnassigned = count(Streams::AssignStream($db_path, 'un'));
		// Streams::UpdateCatCounterByStatus($db_path, "0", $CountUnassigned, 'ALL');

		$this->Data['AllStreams'] 		= $Allstreams;
		$this->Data['StreamBackups'] 	= $StreamBackups;
		$this->Data['AllCategory'] 		= $AllCategories;
		//$this->Data['CountUnassigned'] 	= $CountUnassigned;
		
	}

	public function PlayStream($StreamID)
	{
		$db_path 	= $this->GetDbPath();

		if (!$StreamID)
		{
			$StreamID   = $this->Params[0];
		}
		
		$StreamURL 	= $this->StreamGetUrl($StreamID);

		$this->Data['StreamURL'] = $StreamURL;
	}

	public function StreamGetUrl($StreamID)
	{
		$db_path 		= $this->GetDbPath();
		$PeerID     	= streams::GetPeerID($db_path, $StreamID);
		if ($PeerID == 999)
		{
			$StreamURL = streams::GetSource($db_path, $StreamID);
		}
		else
		{
			$PeerStreamNumb = streams::GetStreamNbr($db_path, $StreamID);
			$PeerURL 		= Peers::GetPeerURL($db_path, $PeerID);		
			$StreamURL 	= $PeerURL . $PeerStreamNumb . '.ts';	
		}
		return $StreamURL;

	}

	public function UpdateCatCounter($db_path, $CatID)
	{
		//Count all streams in this category
		$count 	= count(Streams::AssignStream($db_path, $CatID));
		$status	= 'ALL';
		Streams::UpdateCatCounterByStatus($db_path, $CatID, $count, $status );

		//Read the Number of Online Streams from the category table
		$statusON		= 'ON';
		$CountOnline 	= count(Streams::GetNbrStreamsByCatByStatus($db_path, $CatID, $statusON));
		
		//Read the Number of Runnning Streams from the category table
		$statusACT	  = 'ACT';
		$CountRunning = count(Streams::GetNbrStreamsByCatByStatus($db_path, $CatID, $statusACT));

		Streams::UpdateCatCounterByStatus($db_path, $CatID, $CountOnline, $statusON );
		Streams::UpdateCatCounterByStatus($db_path, $CatID, $CountRunning, $statusACT );
	}

	public function GetName($StreamID)
	{
		$db_path 	= $this->GetDbPath();

		$StreamName = Streams::GetName($db_path, $StreamID);

		$this->Data['StreamName'] = $StreamName;

	}

	public function ViewAllCategory()
	{
		$db_path 		= $this->GetDbPath();
		$AllCategories 	= Streams::AllCategories($db_path);
		usort($AllCategories, function($a, $b) 
		{
	        return strcasecmp($a['TEXT'], $b['TEXT']);
	        //return $a['TEXT'] - $b['TEXT'];
		});
		$this->Data['AllCategory'] = $AllCategories;

	}

	public function ViewCategory()
	{
		$db_path 	= $this->GetDbPath();
		$CategoryID = $this->Params[0];

		$CategoryInfo 	= Streams::ViewCategory($db_path, $CategoryID);
		$StreamList 	= Streams::AssignStream($db_path, $CategoryID);

		$AllCategories 	= Streams::AllCategories($db_path);
		usort($AllCategories, function($a, $b) 
		{
	        return strcasecmp($a['TEXT'], $b['TEXT']);
	        //return $a['TEXT'] - $b['TEXT'];
		});
		

		$this->Data['AllCategory'] 	= $AllCategories;		
		$this->Data['CategoryInfo'] = $CategoryInfo;
		$this->Data['StreamList'] 	= $StreamList;

	}	

	public function Edit()
	{
		$db_path 	= $this->GetDbPath();
		$Object 	= $this->Params[0];
		$CatID 		= $this->Params[1];


		echo $Object;

		if ($Object == "Category")
		{
			if(isset($_POST['inputCategoryName']))
			{
				$CategoryName 			= $_POST['inputCategoryName'];
				$CategoryGroup 			= $_POST['inputCategoryGroup'];
			}
		

		Streams::UpdateCategory($db_path, $CatID, $CategoryName, $CategoryGroup);
		$this->redirect("streams", "editcategory", array($CatID));	

		}

	}

	public function EditCategory()
	{
		$db_path 	= $this->GetDbPath();
		$CategoryID = $this->Params[0];

		$CategoryInfo 	= Streams::ViewCategory($db_path, $CategoryID);
		$AllCategories 	= Streams::AllCategories($db_path);

		$this->Data['CategoryInfo'] = $CategoryInfo;
		$this->Data['AllCategory'] 	= $AllCategories;

	}	

	public function CreateCategory()
	{
		$db_path 		= $this->GetDbPath();
		$AllCategories 	= Streams::AllCategories($db_path);

		$this->Data['AllCategory'] = $AllCategories;
	}	

	public function CreateDefaultCategory()
	{
		$db_path 		= $this->GetDbPath();
		$CategoryName 	= Config::get('cat_un');

		Streams::InsertCategory($db_path, $CategoryName);
	}

	public function Add()
	{
		$db_path 	= $this->GetDbPath();
		$Object 	= $this->Params[0];
		$CatID 		= $this->Params[1];

		if ($Object == "Category")
		{
			if(isset($_POST['inputCategoryName']))
			{
				$CategoryName 			= $_POST['inputCategoryName'];
				$CategoryType 			= $_POST['inputCategoryType'];
				if ($CategoryType === "SUB")
				{
					$CategoryGroup 			= $_POST['inputCategoryGroup'];
				} else
				{
					$CategoryGroup 			= "";
				}
				
			}
		
		Streams::InsertCategory($db_path, $CategoryName, $CategoryGroup);
		$this->redirect("streams", "createcategory", array());	

		}

	}	

	public function DeleteCategory()
	{
		$db_path 	= $this->GetDbPath();
		$CategoryID = $this->Params[0];
		Streams::DeleteCategory($db_path, $CategoryID);
		$this->redirect("streams", "viewallcategory", array());

	}	

	public function AssignCategory()
	{
		$db_path 		= $this->GetDbPath();
		$StreamSel 		= $this->Params[0];

		if (!empty($StreamSel))
		{
			$StreamList 	= Streams::AssignStream($db_path, $StreamSel);
		}
		else
		{
			$StreamList 	= Streams::YourStream($db_path);
		}

			
		usort($StreamList, function($a, $b) 
		{
        	return strcasecmp($a['STREAM_NAME'], $b['STREAM_NAME']);
        	//return $a['STREAM_NAME'] - $b['STREAM_NAME'];
		});

		$AllCategories 	= Streams::AllCategories($db_path);

		$this->Data['YourStreams'] = $StreamList;
		$this->Data['AllCategory'] = $AllCategories;
	}	

	public function UpdateCategory()
	{
		$db_path 		= $this->GetDbPath();
		$StreamID   	= $this->Params[0];
		$CategoryID 	= $this->Params[1];
		$RedirectView 	= $this->Params[2];
		$RedirectParam	= $this->Params[3];

		if (empty($RedirectView))
		{
			$RedirectView = "assigncategory";
		}

		Streams::UpdateStreamCategory($db_path, $StreamID, $CategoryID);
		$this->UpdateCatCounter($db_path, $CategoryID);
		$this->UpdateCatCounter($db_path, $RedirectParam);


		$this->redirect("streams", $RedirectView, array($RedirectParam));		
	}

	public function GetStreamCategory($StreamID)
	{
		$db_path   = $this->GetDbPath();

		$StreamCat = Streams::Getcategory($db_path, $StreamID);

		$this->Data['StreamCat'] = $StreamCat;

		return $StreamCat;
	}

	public function StreamStatus($StreamID)
	{
		$db_path 	= $this->GetDbPath();

		$StreamsSrc = $this->StreamSrc($StreamID);
		$stream_chk = shell_exec ( "ffprobe -v quiet -print_format json -show_streams '$StreamsSrc' " );
		//$stream_chk = shell_exec ( "ffprobe '$StreamsSrc' " );

		if (json_decode($stream_chk, true))
		{
			$Status = "ON";
		} else
		{
			$Status = "OFF";
		}

		streams::StreamStatus($db_path, $StreamID, $Status);

		//return $messages;
	}

	public function StreamStatusCat($CatID)
	{
		$db_path 	= $this->GetDbPath();

		$AllStreamsIDbyCat = Streams::GetAllStreamsIDbyCat($db_path, $CatID);

		usort($AllStreamsIDbyCat, function($a, $b) 
		{
    		return $a['ID'] - $b['ID'];
		});		
		
		foreach ($AllStreamsIDbyCat as $StreamID) 
		{
			$this->StreamStatus($StreamID["ID"]);
			$messages[] = ["I", STREAM_STATUS_MSG_001 . "[" . $StreamID["ID"] ."]" .  STREAM_STATUS_MSG_002];
		}	

		$this->UpdateCatCounter($db_path, $CatID);

		return $messages;
	}

	public function StreamStatusAll()
	{
		$db_path 	= $this->GetDbPath();

		$AllCategories = Streams::AllCategories($db_path);

		usort($AllCategories, function($a, $b) 
		{
        	return $a['ID'] - $b['ID'];
		});

		foreach ($AllCategories as $Cat) 
		{
			$Redirect = 0;
			$this->StreamStatusCat($Cat["ID"], $Redirect);
		}	

		return $messages;
	}

	public function StreamStatusMap($StreamID)
	{
		$db_path 	= $this->GetDbPath();

		$AllStreamsMapped = Streams::GetAllStreamsMapped($db_path, $StreamID);

		usort($AllStreamsMapped, function($a, $b) 
		{
			return $a['ID'] - $b['ID'];
		});		

		foreach ($AllStreamsMapped as $Stream) 
		{
			$this->StreamStatus($Stream["ID"]);
			$messages[] = ["I", STREAM_STATUS_MSG_001 . "[" . $Stream["ID"] ."]" .  STREAM_STATUS_MSG_002];
		}

		return $messages;
	}

	public function status()
	{
		global $_SESSION;
		session_start();
		$messages  = array();

		$Param1 = $this->Params[0]; // "cat", "all"
		$Param2 = $this->Params[1]; // "map"

		if ($Param1)
		{
			if ($Param1 === "cat")
			{
				if ($Param2)
				{
					$messages = $this->StreamStatusCat($Param2);
					$this->redirect("streams", "", array(), $messages);					
				} else
				{
					$messages[] = ["E", STREAM_STATUS_MSG_003];
					$this->redirect("streams", "", array(), $messages);	
				}


			} elseif ($Param1 === "all")
			{
				$messages = $this->StreamStatusAll();
				$this->redirect("streams", "", array(), $messages);
			} else
			{
				if ($Param2)
				{
					if ($Param2 === "map")
					{
						$messages = $this->StreamStatusMap($Param1);
						$this->redirect("streams", "", array(), $messages);

					} else
					{
						$this->StreamStatus($Param1);
						$messages[] = ["I", STREAM_STATUS_MSG_001 . "[" . $Param1 ."]" .  STREAM_STATUS_MSG_002];
						$this->redirect("streams", "", array(), $messages);
					}
				} else
				{
					$this->StreamStatus($Param1);
					$messages[] = ["I", STREAM_STATUS_MSG_001 . "[" . $Param1 ."]" .  STREAM_STATUS_MSG_002];
					$this->redirect("streams", "", array(), $messages);					
				}
			}

		}

	}

	public function QualityStream($StreamID)
	{
		$db_path 	= $this->GetDbPath();
		$StreamsSrc = $this->StreamSrc($StreamID);
		// echo  "ffprobe -v quiet -print_format json -show_streams " . "$StreamsSrc";
		$stream_chk = shell_exec ( "ffprobe -v quiet -print_format json -show_streams '$StreamsSrc' " );

		if (json_decode($stream_chk, true))
		{
			$pieces = explode(",", $stream_chk);
			$VideoCodec = str_replace('"', '', explode(":", $pieces[1])[1]);	
			$AudioCodec = str_replace('"', '', explode(":", $pieces[40])[1]);
			$BitRate = str_replace('"', '', explode(":", $pieces[8])[1]) . " x " . str_replace('"', '', explode(":", $pieces[9])[1]);

			streams::StreamVideoCodec($db_path, $StreamID, $VideoCodec);
			streams::StreamAudioCodec($db_path, $StreamID, $AudioCodec);
			streams::StreamBitRate($db_path, $StreamID, $BitRate);
		} 
	}

	public function QualityCategory($CatID)
	{

		$db_path 	= $this->GetDbPath();
		$AllStreamsIDbyCat = Streams::GetAllStreamsIDbyCat($db_path, $CatID);

		usort($AllStreamsIDbyCat, function($a, $b) 
		{
    		return $a['ID'] - $b['ID'];
		});		
		
		foreach ($AllStreamsIDbyCat as $Stream) 
		{
			$this->QualityStream($Stream['ID']);
			$messages[] = ["I", STREAM_QUA_MSG_001 . "[" . $Stream['ID'] ."]" .  STREAM_QUA_MSG_002];
		}	

		//$this->redirect("streams", "", array(), $messages);
	}

	public function QualityAll()
	{
		$db_path 	= $this->GetDbPath();
		$AllStreamsID = Streams::GetAllStreamsID($db_path);

		usort($AllStreamsID, function($a, $b) 
		{
    		return $a['ID'] - $b['ID'];
		});

		foreach ($AllStreamsID as $Stream) 
		{
			$this->QualityStream($Stream['ID']);
			$messages[] = ["I", STREAM_QUA_MSG_001 . "[" . $Stream['ID'] ."]" .  STREAM_QUA_MSG_002];
		}

		//$this->redirect("streams", "", array(), $messages);		
	}

	public function QualityMap($StreamID)
	{
		$db_path 	= $this->GetDbPath();
		//Find all streams mapped to that one
		$AllStreamsMapped = Streams::GetAllStreamsMapped($db_path, $StreamID);

		usort($AllStreamsMapped, function($a, $b) 
		{
			return $a['ID'] - $b['ID'];
		});		

		foreach ($AllStreamsMapped as $Stream) 
		{
			$this->QualityStream($Stream['ID']);
			$messages[] = ["I", STREAM_QUA_MSG_001 . "[" . $Stream['ID'] ."]" .  STREAM_QUA_MSG_002];
		}			

		//$this->redirect("streams", "", array(), $messages);	
	}


	public function quality($StreamID = 0, $CatID = 0, $Option = 0  )
	{
		global $_SESSION;
		$this->SessionStarted();

		$messages  = array();
		$db_path 	= $this->GetDbPath();


		if ($this->Params[0])
		{
			if ($this->Params[0] === 'cat')
			{
				$CatID = $this->Params[1];
				$this->QualityCategory($CatID);
				$this->redirect("streams", "", array(), $messages);

			} elseif ($this->Params[0] === 'all')
			{
				$this->QualityAll();
				$this->redirect("streams", "", array(), $messages);
			} else //this is a streamID
			{
				$StreamID = $this->Params[0];

				if ($this->Params[1])
				{
					if ($this->Params[1] === 'map')
					{
						$this->QualityMap($StreamID);
						$this->redirect("streams", "", array(), $messages);
					}

				} else
				{
					$this->QualityStream($StreamID);
					$this->redirect("streams", "", array(), $messages);	
				}

			}
		}		
	}	

	public function StreamSrc($StreamID = 0)
	{
		
		$db_path 	= $this->GetDbPath();
		$PeerID		= Streams::GetPeerID($db_path, $StreamID);

		if ($PeerID	=== 0)
		{
			$port 		= Config::get('stream_1st_port') + $StreamID;
			$url 		= Config::get('stream_svr_url');
			$StreamSrc 	= $url . ":" . $port;	
		}
		elseif ($PeerID	=== 999)
		{
			$StreamSrc 	= Streams::GetSource($db_path, $StreamID);
		}
		else
		{
			$PeerURL 	= Peers::GetPeerURL($db_path, $PeerID);
			$StreamNbr  = Streams::GetStreamNbr($db_path, $StreamID);
			$StreamSrc 	= $PeerURL . $StreamNbr . ".ts";
		}

		return $StreamSrc;
	}

	public function SelectBestPeerStream($StreamID)
	{
		$db_path 	= $this->GetDbPath();
		//Select all PeerStream
		$AllStreamsMapped = Streams::GetAllStreamsMapped($db_path, $StreamID);
		//Check status/quality 
		foreach ($AllStreamsMapped as $Stream) 
		{
			$this->StreamStatus($Stream["ID"]);
			$this->QualityStream($Stream["ID"]);
			//$this->status($Stream["ID"], " ", "no");			
			//$this->quality($Stream["ID"], " ", "no");
		}
		//Reload Online Stream data from db
		$AllStreamsOnlineMapped = Streams::GetAllStreamsOnlineMapped($db_path, $StreamID);
		//Sort stream by BitRate

		//var_dump($AllStreamsOnlineMapped);
		usort($AllStreamsOnlineMapped, array('StreamsController', 'CompQuality'));

		//Select 1st one
		return $AllStreamsMapped[0];
	}

	private static function CompQuality($a, $b)
	{
		$CalcBitrate_a = intval(explode("x", $a['STREAM_QA'])[0]) * intval(explode("x", $a['STREAM_QA'])[1]) ;
		$CalcBitrate_b = intval(explode("x", $b['STREAM_QA'])[0]) * intval(explode("x", $b['STREAM_QA'])[1]) ;
		// echo "CalcBitrate_a: " . $CalcBitrate_a . "<br>"; 
		// echo "CalcBitrate_b: " . $CalcBitrate_b . "<br>"; 

		$a = $CalcBitrate_a;
		$b = $CalcBitrate_b;

		if ($a == $b) {
			return 0;
		}
		return ($a < $b) ? -1 : 1;
	}

	public function vlc_command($StreamsSrc, $port)
	{
		$cmd = "sudo -u ". Config::get('engine-user') . " ". Config::get('vlc_engine-cmd') . " -I dummy " . $StreamsSrc ." :network-caching=". Config::get('network-caching'). " --verbose=0 --file-logging --logfile=". Config::get('logfile') . $port . ".log --sout ";
		$cmd = $cmd . "'#standard{access=http,mux=ts,dst=localhost:" . $port . "}' ";
		//$cmd = $cmd . ":sout-keep vlc://quit -q >/dev/null 2>/dev/null & jobs -p";
		$cmd = $cmd . ":sout-keep vlc://quit -q >/dev/null 2>/dev/null & jobs -p";

		return $cmd;
	}

	public function ffmpeg_cmd($StreamsSrc, $port)
	{
		$cmd = "sudo -u ". Config::get('engine-user') . " ". Config::get('ffmpeg_engine-cmd');
		//$cmd_src_opt = " -start_at_zero -copyts -vsync 0 -y -nostats -nostdin -hide_banner -loglevel quiet -probesize 10000000 -analyzeduration 10000000 -fflags +genpts ";
		$cmd_src_opt = " -start_at_zero -copyts -vsync 0 -y -nostats -nostdin -hide_banner -loglevel quiet -probesize 10000000 -analyzeduration 10000000 -fflags +genpts ";
		$cmd_agent 	= " -user_agent " . Config::get('streaming_agent');
		$cmd_source = " -i " . $StreamsSrc;
		$cmd_option = " -strict -2 -dn -c copy ";
		$cmd_hls	= " -hls_flags delete_segments -hls_time 10 -hls_list_size 6 ";
		$cmd_target = Config::get('tmpfile') . $port . "_.m3u8";
		$cmd_background = " > /dev/null 2>&1 & echo $!";

		$cmd = $cmd . $cmd_src_opt . $cmd_agent . $cmd_source . $cmd_option . $cmd_hls . $cmd_target . $cmd_background;

		return $cmd;
	}

	public function StartStream($StreamID)
	{

		$db_path 	= $this->GetDbPath();

		$BestPeerStream = $this->SelectBestPeerStream($StreamID);
		//echo $BestPeerStream[0];

		if ($BestPeerStream)
		{
			$messages[] = ["I", STREAM_START_MSG_005 . "[" . $BestPeerStream[0] ."]" . STREAM_START_MSG_006 . "[" . $StreamID ."]"];
			//Find the streamUrl
			$StreamsSrc = $this->StreamSrc($BestPeerStream[0]);

			if ($StreamsSrc)
			{
				$port = Config::get('stream_1st_port') + $StreamID;

				$engine = $this->CheckEngine();

				if ($engine)
				{
					if ($engine === "vlc")
					{
						$cmd = $this->vlc_command($StreamsSrc, $port);
					} elseif ($engine === "ffmpeg")
					{
						$cmd = $this->ffmpeg_cmd($StreamsSrc, $port);
					} else
					{
						$messages[] = ["W", STREAM_NOT_STARTED . ":" . STREAM_UNDEFINED_ENGINE ];
					}

					var_dump($cmd);
					

					$streampid = shell_exec ( $cmd );

					if ($streampid) 
					{
						$messages[] = ["I", STREAM_STARTED . "[" . $StreamID ."]"];
						$messages[] = ["I", STREAM_START_MSG_003 . "[" . $BestPeerStream[0] ."]"];
						//Update the STREAM_STATUS to 'ACT' or 'RUN'
							$Status = 'ACT';
							streams::StreamStatus($db_path, $StreamID, $Status);  			//Main StreamStatus
							$messages[] = ["I", STREAM_STATUS_MSG_004 . "[" . $StreamID ."]"];
							streams::StreamStatus($db_path, $BestPeerStream[0], $Status);  	//Peer StreamStatus
							$messages[] = ["I", STREAM_STATUS_MSG_005 . "[" . $BestPeerStream[0] ."]"];

						//Save the streamPID
							Streams::UpdateStreamPID($db_path, $BestPeerStream[0], $streampid);		
							$messages[] = ["I", STREAM_PID . "[" . $streampid ."]"];
							
						//Update the category counter.
							$CatID	= $this->GetStreamCategory($StreamID);
						//echo $CatID;
							$this->StreamStatusCat($CatID);
					} else
					{
						$messages[] = ["W", STREAM_NOT_STARTED . "[" . $BestPeerStream[0] ."]"];
					}

				} else
				{
					$messages[] = ["W", STREAM_NOT_STARTED . ":" . STREAM_NO_ENGINE ];
				}

			} else
			{
				$messages[] = ["W", STREAM_NOT_STARTED . ":" . STREAM_NO_SOURCE ];
			}

		} else
		{
			$messages[] = ["E", STREAM_START_MSG_004 . STREAM_START_MSG_005 .  "[" . $StreamID ."]"];
		}

		return $messages;
	}

	public function StartStreamCat($CatID)
	{
		$db_path 	= $this->GetDbPath();
		$AllStreamsIDbyCat = Streams::GetAllStreamsIDbyCat($db_path, $CatID);

		usort($AllStreamsIDbyCat, function($a, $b) 
		{
    		return $a['ID'] - $b['ID'];
		});		
		
		//var_dump($AllStreamsIDbyCat);
		foreach ($AllStreamsIDbyCat as $StreamID) 
		{
			$messages = $this->StartStream($StreamID["ID"]);
		}	

		return $messages;
	}	

	public function StartStreamAll()
	{
		$AllStreamsID = Streams::GetAllStreamsID($db_path);

		usort($AllStreamsID, function($a, $b) 
		{
    		return $a['ID'] - $b['ID'];
		});

		foreach ($AllStreamsID as $StreamID) 
		{
			$messages = $this->StartStream($StreamID['ID']);
		}

		return $messages;
	}	

	public function StartStreamMap($StreamID)
	{

	}		

	public function Start()
	{
		global $_SESSION;		
		$this->SessionStarted();
		
		$db_path 	= $this->GetDbPath();
		$messages  = array();

		$Param1 = $this->Params[0];
		$Param2 = $this->Params[1];

		if ($Param1)
		{
			if ($Param1 === "cat")
			{
				if ($Param2)
				{
					$messages = $this->StartStreamCat($Param2);
					$this->redirect("streams", "", array(), $messages);
				} else
				{
					$messages[] = ["E", STREAM_START_MSG_001];
					$this->redirect("streams", "", array(), $messages);
				}

			} elseif ($Param1 === "all")
			{
				$messages = $this->StartStreamAll();
				$this->redirect("streams", "", array(), $messages);
			} else
			{
				if ($Param2)
				{
					// if ($Param2 === "map")
					// {
					// 	$messages = $this->StartStreamAll($Param1);
					// 	$this->redirect("streams", "", array(), $messages);
					// } else
					// {
					// 	$messages[] = ["E", STREAM_START_MSG_002];
					// 	$this->redirect("streams", "", array(), $messages);
					// }
				} else
				{
					$messages = $this->StartStream($Param1);
					//$this->redirect("streams", "", array(), $messages);
				}
			}

		}

	}

	public function AddSource()
	{
		$db_path 	= $this->GetDbPath();
		$StreamID   = $this->Params[0];

		$this->GetName($StreamID);
		$this->GetStreamCategory($StreamID);
		$this->Data['StreamID'] 	= $StreamID;
	}

	public function AddSourceForm()
	{
		global $_SESSION;
		session_start();

		$db_path 	= $this->GetDbPath();
		$messages  	= array();

		//var_dump($_POST);

		if(isset($_POST['inputStreamName']))
		{
			$MapStreamID 			= $_POST['inputMapStreamID'];
			$StreamName 			= $_POST['inputStreamName'];
			$StreamCategory			= $_POST['inputStreamCategory'];
			$StreamSource			= $_POST['InputStreamSource'];
		}	

		$PeerID = "999";
		Streams::InsertStreamSource($db_path, $StreamName, $StreamCategory, $StreamSource, $MapStreamID, $PeerID);

		$messages[] = ["I", addsource_MSG_001 . " [" . $StreamName ."]"];			

		$this->redirect("streams", "", array(), $messages);

	}

	public function CreateStream()
	{
		$db_path 	= $this->GetDbPath();
		$StreamID   = $this->Params[0];

		$AllCategories 				= Streams::AllCategories($db_path);
		$StreamList 				= Streams::YourStream($db_path);
		$this->Data['AllCategory'] 	= $AllCategories;
		$this->Data['YourStream'] 	= $StreamList;
		$this->GetName($StreamID);
		$this->Data['StreamID'] 	= $StreamID;

		//Streams::InsertNewStream($db_path);
	}	

	public function CreateStreamForm()
	{

		$db_path 	= $this->GetDbPath();
		//$StreamID   = $this->Params[0];
		$messages  	= array();


		//var_dump($_POST);
		if(isset($_POST['inputStreamName']))
		{
			$MapStreamID 			= $_POST['inputMapStreamID'];
			$StreamName 			= $_POST['inputStreamName'];
			$StreamCategory			= $_POST['inputStreamCategory'];
			$StreamType				= $_POST['inputStreamType'];
			$ExisitingStream		= $_POST['inputExistingStream'];
		}		

		if ($StreamType === "NEW")
		{
			$StreamCreated = Streams::InsertNewStream($db_path, $StreamName, $StreamCategory);

			if ($StreamCreated)
			{
				Streams::UpdateMapping($db_path, $MapStreamID, $StreamCreated);
			}			
		} else
		{
			$StreamSource 	= "";
			$PeerID 		= Streams::GetPeerID($db_path, $MapStreamID);
			$StreamCategory = Streams::Getcategory($db_path, $ExisitingStream);
			Streams::InsertStreamSource($db_path, $StreamName, $StreamCategory, $StreamSource, $ExisitingStream, $PeerID);
		}


	
		$AllCategories = Streams::AllCategories($db_path);
		foreach ($AllCategories as $Cat) 
		{
			$this->UpdateCatCounter($db_path, $Cat["ID"]);
		}

		$messages[] = ["I", STREAM_INS . "[" . $StreamCreated ."]"];
		$messages[] = ["I", STREAM_MAP_TO . "[" . $MapStreamID ."]"];
		$messages[] = ["I", STREAM_INS_CAT . "[" . $StreamCategory ."]"];

		$this->redirect("streams", "", array(), $messages);
	}	

	public function DeleteMapping()
	{

		global $_SESSION;
		session_start();		
		
		$db_path 		= $this->GetDbPath();
		$YourStream 	= $this->Params[0];
		$PeerStream 	= $this->Params[1];

		if ($PeerStream == "map")
		{

			$YourStreamUpd = null;

			$AllMappedStreams = Streams::GetAllStreamsMapped($db_path, $YourStream);

			foreach ($AllMappedStreams as $MappedStream) 
			{
				Streams::UpdateMapping($db_path, $MappedStream['ID'], $YourStreamUpd);
			}

			$messages[] = ["I", deletemapping_MSG_003 . " [" . $YourStream ."]" ];

			$this->redirect("streams", "", array(), $messages);

		} 
		else
		{
			$YourStreamUpd = null;
			Streams::UpdateMapping($db_path, $YourStream, $YourStreamUpd);

			$messages[] = ["I", deletemapping_MSG_001 . " [" . $PeerStream ."]" . deletemapping_MSG_002 . " [" . $YourStream ."]"];

			$this->redirect("streams", "", array(), $messages);			
		}
	}

	public function DeleteStream()
	{
		global $_SESSION;
		session_start();

		$db_path 		= $this->GetDbPath();
		$StreamID 		= $this->Params[0];

		if ($this->Params[1] == "map")
		{
			$AllMappedStreams = Streams::GetAllStreamsMapped($db_path, $StreamID);
			foreach ($AllMappedStreams as $MappedStream) 
			{
				Streams::DeleteStream($db_path, $MappedStream['ID']);
			}	
			
			$messages[] = ["I", deletestream_MSG_002 . " [" . $StreamID ."]" ];	

			$this->redirect("streams", "", array(), $messages);		
		}
		else
		{
			$CatID	= $this->GetStreamCategory($StreamID);
			Streams::DeleteStream($db_path, $StreamID);
			$this->UpdateCatCounter($db_path, $CatID);

			$messages[] = ["I", deletestream_MSG_001 . " [" . $StreamID ."]" ];

			$this->redirect("streams", "", array(), $messages);			
		}

	}	

	public function StopStream($StreamID)
	{
		$db_path 		= $this->GetDbPath();
		//Get the Peer streams
		$PeerStreams 	= Streams::FindPeerStreams($db_path, $StreamID);
		$messages 		= array();
		
		foreach ($PeerStreams as $PeerStream)
		{
			//Get the PID
				$streampid = Streams::GetStreamPID($db_path, $PeerStream['ID']);
				$streampid = $streampid + 1;
			//Stop the PID
				$cmd = "kill " . $streampid;
				//echo $cmd  . "<br>";
				shell_exec("$cmd");	
			//Set the stream status	
				$this->StreamStatus($PeerStream['ID']);				
			//Reset the StreamPID
				$streampid = "";
				Streams::UpdateStreamPID($db_path, $PeerStream['ID'], $streampid);	
			//Collect All Messages
				$messages[] = ["S", STREAM_STOPPED . " [" . $PeerStream['ID'] ."]"];
				
		}
		//Update the category counter.
			$CatID	= $this->GetStreamCategory($StreamID);
			//echo $CatID;
			//$this->status("cat", $CatID);	
			$messages = $this->StreamStatusCat($CatID);

		//Set the stream status	
			//$this->status($StreamID, "no");
			$messages = $this->StreamStatus($StreamID);
		//Add Message
		$messages[] = ["S", STREAM_STOPPED . " [" . $StreamID ."]"];

		return $messages;
	}

	public function StopStreamCat($CatID)
	{

		$db_path 			= $this->GetDbPath();
		$AllStreamsIDbyCat 	= Streams::GetAllStreamsIDbyCat($db_path, $CatID);

		usort($AllStreamsIDbyCat, function($a, $b) 
		{
    		return $a['ID'] - $b['ID'];
		});		
		
		foreach ($AllStreamsIDbyCat as $StreamID) 
		{
			$messages = $this->StopStream($StreamID["ID"]);
		}	

		return $messages;
	}

	public function StopStreamAll()
	{
		$db_path 			= $this->GetDbPath();
		$AllCategories = Streams::AllCategories($db_path);

		usort($AllCategories, function($a, $b) 
		{
        	return $a['ID'] - $b['ID'];
		});

		foreach ($AllCategories as $Cat) 
		{
			$messages = $this->StopStreamCat($Cat["ID"]);
		}

		return $messages;
	}

	public function Stop()
	{
		global $_SESSION;
		session_start();		
		$db_path 	= $this->GetDbPath();
		$messages  = array();

		$Param1 = $this->Params[0];
		$Param2 = $this->Params[1];

		if ($Param1)
		{
			if ($Param1 === "cat")
			{
				if ($Param2)
				{
					$messages = $this->StopStreamCat($Param2);
					$this->redirect("streams", "", array(), $messages);
				} else
				{
					$messages[] = ["E", STREAM_START_MSG_001];
					$this->redirect("streams", "", array(), $messages);
				}

			} elseif ($Param1 === "all")
			{
				$messages = $this->StopStreamAll();
				$this->redirect("streams", "", array(), $messages);
			} else
			{
				if ($Param2)
				{
					// if ($Param2 === "map")
					// {
					// 	$messages = $this->StopStreamMap($Param1);
					// 	$this->redirect("streams", "", array(), $messages);
					// } else
					// {
					// 	$messages[] = ["E", STREAM_START_MSG_002];
					// 	$this->redirect("streams", "", array(), $messages);
					// }
				} else
				{
					$messages = $this->StopStream($Param1);
					$this->redirect("streams", "", array(), $messages);
				}
			}

		}

	}	
	
}