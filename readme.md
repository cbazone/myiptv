# MyIPTV Panel #

## Update Instructions ##
* Rename/Backup db
If you are using the default db, make a copy or it will be overwritten.

* Clone the repository to get the latest code

```
#!text
cd /var/www/..
git clone https://cbazone@bitbucket.org/cbazone/myiptv.git
```
This will update the folder www/var/myiptv


## Installation Instructions ##

### Apache Configuration ###

* Install Apache
```
#!text
$ sudo apt-get install apache2 apache2-utils
```

* Install PHP

For PHP 5
```
#!text
$ sudo apt-get install php5 php5-cli
```
For PHP 7
```
#!text
$ sudo apt install php7.0-cli php7.0-common php7.0-curl php7.0-opcache php7.0-json php7.0-gd
```


* Enable RewritingMod
```
#!text
$ sudo a2enmod rewrite
```

* Enable support for SQLlite3
```
#!text
$ sudo apt-get install php5-sqlite
```
For PHP7
```
#!text
$ sudo apt install php7.0-sqlite3 
```

* Install git
```
#!text
$ sudo apt-get install git
```

* Install FFmpeg 2.3.3
```
#!text
$ sudo add-apt-repository ppa:mc3man/trusty-media
$ sudo apt-get update
$ sudo apt-get install ffmpeg
``` 
This is needed because the panel use ffprobe to check the stream status/quality.

* Install VLC
```
#!text
$ sudo apt-get install vlc
``` 

* Allow WebUser to run vlc and cvlc
```
#!text
$ echo 'www-data ALL=(ALL) NOPASSWD: /usr/bin/vlc' >> /etc/sudoers
$ echo 'www-data ALL=(ALL) NOPASSWD: /usr/bin/cvlc' >> /etc/sudoers
$ echo 'www-data ALL=(ALL) NOPASSWD: /usr/bin/ffmpeg' >> /etc/sudoers
``` 
If you prefer, you can do it manually with 
```
#!text
$ sudo visudo
``` 

* Download & Move app in apache folder
```
#!text
cd /var/www/..
git clone https://cbazone@bitbucket.org/cbazone/myiptv.git
```
This will create the folder www/var/myiptv

* Adjust authorization on the folder
Webserver user = www-data, adjust to your config
Default MyIPTV folder = myiptv, adjust to your config
```
#!text
chown -R www-data myiptv
chgrp -R www-data myiptv
```

* Allow the UrlRewritting for that folder in apache2.conf or in the virtualHost
```
#!text
<Directory /var/www/..>
        ...
        AllowOverride All
        ...
</Directory>
```

* Example of VirtualHost
```
#!text
<VirtualHost *:80>

	#ServerName www.example.com
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/myiptv/

    <Directory /var/www/myiptv/>
            Options FollowSymlinks
            DirectoryIndex index.php
            AllowOverride All
            Require all granted
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
```

### php5-FPM configuration ###
This is optional the goal is to reduce the resource used by apache

* Install libapache2-mod-fastcgi, php5-fpm & apache2-mpm-worker
```
#!text
apt-get install libapache2-mod-fastcgi, php5-fpm apache2-mpm-worker
```

* Disable PHP in apache
```
#!text
a2dismod php5 mpm_prefork
```

* Enable mpm_worker
```
#!text
a2enmod mpm_worker
```

* Enable ProxyModules
```
#!text
a2enmod proxy
a2enmod proxy_fcgi
```

* Enable mod_actions Module
```
#!text
a2enmod actions
```

* Check your apache version
```
#!text
apache2 -v
```

* Depending on your Apache version, Update /etc/apache2/mods-enabled/fastcgi.conf

in Apache 2.2
```
#!text
<IfModule mod_fastcgi.c>
 AddType application/x-httpd-fastphp5 .php
 Action application/x-httpd-fastphp5 /php5-fcgi
 Alias /php5-fcgi /usr/lib/cgi-bin/php5-fcgi
 FastCgiExternalServer /usr/lib/cgi-bin/php5-fcgi -socket /var/run/php5-fpm.sock -pass-header Authorization
</IfModule>
```

in Apache 2.4
```
#!text
<IfModule mod_fastcgi.c>
 AddType application/x-httpd-fastphp5 .php
 Action application/x-httpd-fastphp5 /php5-fcgi
 Alias /php5-fcgi /usr/lib/cgi-bin/php5-fcgi
 FastCgiExternalServer /usr/lib/cgi-bin/php5-fcgi -socket /var/run/php5-fpm.sock -pass-header Authorization
 <Directory /usr/lib/cgi-bin>
  Require all granted
 </Directory>
</IfModule>
```

* Restart Apache and php5-fpm 
```
#!text
service apache2 restart
service php5-fpm restart
```

* Check you are using PHP5-FPM
```
#!text
echo "<?php phpinfo(); ?>" > /var/www/myiptv/webroot/info.php
```
Open your browser to http://<yourIP>/info.php
Your should see your are using php5-FPM

### Panel Configuration ###

* Update the config files in /config
```
#!text
cp config_template.php config.php
```
Adjust the needed parameters

* Start Your Browser & point to http://<yourIP>; your should get the login page
Default User is 'Admin' with password 'Admin' this can be checked in the users table of the db.
Please update the password, this can be done using the Panel.

In case of issues, please start checking the apache log.