<?php

define("SERVEROS", "Système d'exploitation");
define("SERVERIP", "IP du Server");
define("SERVERINFORMATION", "Informations du Server");
define("GeneralInformation", "Informations Générales");


define("STREAM_INS", "Stream Inséré");
define("STREAM_MAP_TO", "Stream lié au stream");
define("STREAM_INS_CAT", "Stream Inclu dans la catégorie");



//addsource.html
define("addsource_001", "TBT Stream Creation");
define("addsource_002", "TBT StreamID");
define("addsource_003", "TBT StreamID automatically generated");
define("addsource_004", "TBT Mapped to Stream");
define("addsource_005", "TBT Enter the Mapped Stream ID");
define("addsource_006", "TBT StreamName");
define("addsource_007", "TBT Enter the Stream Name");
define("addsource_008", "TBT StreamCategory");
define("addsource_009", "TBT StreamSource");
define("addsource_010", "TBT Enter the Stream Source Url");
define("addsource_011", "TBT Add this Stream's source");