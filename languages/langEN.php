<?php

// page index
define("SERVEROS", "Operating System");
define("SERVERKERNEL", "Server Kernel");
define("SERVERIP", "Server IP");
define("SERVERINFORMATION", "Server Information");
define("GeneralInformation", "General Information");
define("ONLINE", "OnLine");
define("ACTIVE", "Active");
define("NOTACTIVE", "Not Active");
define("TOTALCLIENTS", "Total Users");
define("TOTALUSERS", "Total Users");
define("TOTALPEERS", "Total Peers");

define("ONLINE_SERVER_STREAMS", "Online");
define("ACTIVE_SERVER_STREAMS", "Active");
define("TOTAL_SERVER_STREAMS", "Total Server Streams");

define("ONLINE_PEER_STREAM", "Online");
define("MAP_PEER_STREAM", "Map");
define("IGN_PEER_STREAM", "Ign");
define("TOTAL_PEER_STREAMS", "Total Peer Streams");

define("ServerUpTime", "Server UpTime");
define("MemoryUsage", "Memory Usage");
define("LoadAverage", "Load Average");
define("TotalNetworkUsage", "Total Network Usage");
define("UPLOAD", "Upload");
define("DOWNLOAD", "Download");
define("DAY", "Day(s)");
define("HOUR", "Hour(s)");
define("MINUTE", "Minute(s)");
define("SECOND", "Second(s)");

define("DISCONNECT_MSG", "You are now disconnected");
define("CONNECT_MSG", "Your are now connected");
define("CONNECT_ERR1", "Incorrect User or Password");
define("CONNECT_ERR2", "Incorrect User or Password");

define("USER_UPDATED", "User information updated");
define("USER_DEACTIVATED", "User deactivated");
define("USER_ACTIVATED", "User (re)activated");
define("USER_CREATED", "User created");
define("USER_DELETED", "User deleted");

// Streams Page
define("STREAM_STOPPED", "Stream Stopped");
define("STREAM_STARTED", "Stream Started");
define("STREAM_NOT_STARTED", "Stream Not Started");
define("STREAM_NO_SOURCE", "No Source for stream found");
define("STREAM_NO_ENGINE", "No Engine defined");
define("STREAM_UNDEFINED_ENGINE", "Unknwon Engine");

define("STREAM_INS", "Stream Inserted");
define("STREAM_MAP_TO", "Stream Mapped to");
define("STREAM_INS_CAT", "Stream Inserted in Cat");

//addsource.html
define("addsource_001", "Stream Creation");
define("addsource_002", "StreamID");
define("addsource_003", "StreamID automatically generated");
define("addsource_004", "Mapped to Stream");
define("addsource_005", "Enter the Mapped Stream ID");
define("addsource_006", "StreamName");
define("addsource_007", "Enter the Stream Name");
define("addsource_008", "StreamCategory");
define("addsource_009", "StreamSource");
define("addsource_010", "Enter the Stream Source Url");
define("addsource_011", "Add this Stream's source");

define("addsource_MSG_001", "New Source added to stream");

// deletemapping
define("deletemapping_MSG_001", "Mapping deleted between stream ");
define("deletemapping_MSG_002", " and stream ");
define("deletemapping_MSG_003", " Removing all mapping to stream  ");

// deletestream
define("deletestream_MSG_001", "Stream deleted ");
define("deletestream_MSG_002", "Deleted all streams mapped to ");


// Stream Status
define("STREAM_STATUS_MSG_001", "Stream: ");
define("STREAM_STATUS_MSG_002", " Status Updated ");
define("STREAM_STATUS_MSG_003", " Cannot updated status, No category Provided");
define("STREAM_STATUS_MSG_004", " Main Stream status updated");
define("STREAM_STATUS_MSG_005", " Peer Stream status updated");


// Stream Start
define("STREAM_START_MSG_001", " Cannot start stream, No category Provided");
define("STREAM_START_MSG_002", " Nothing done, missing parameter");
define("STREAM_START_MSG_003", " Best Peer Stream Selected and started");
define("STREAM_START_MSG_004", " Cannot select the");
define("STREAM_START_MSG_005", " BestStreamID");
define("STREAM_START_MSG_006", " for StreamID");

// Stream Quality
define("STREAM_QUA_MSG_001", "Stream: ");
define("STREAM_QUA_MSG_002", " Quality checked");

// createcategory.html
define("CREATE_CAT_001", "Category Creation: ");
define("CREATE_CAT_002", "CategoryID: ");
define("CREATE_CAT_003", "CategoryName: ");
define("CREATE_CAT_004", "Add this Category : ");
define("CREATE_CAT_005", "List of categories: ");
define("CREATE_CAT_006", "Name");
define("CREATE_CAT_007", "Group");
define("CREATE_CAT_008", "NumberOfStreams");
define("CREATE_CAT_009", "View");
define("CREATE_CAT_010", "Edit");
define("CREATE_CAT_011", "Delete");
define("CREATE_CAT_012", "CategoryID automatically generated");
define("CREATE_CAT_013", "Enter the Category Name");
define("CREATE_CAT_014", "Select the CategoryGroup");
define("CREATE_CAT_016", "CategoryType");
define("CREATE_CAT_015", "CategoryGroup");
define("CREATE_CAT_017", "New CategoryGroup");
define("CREATE_CAT_018", "Sub CategoryGroup");

// createstream.html
define("CREATE_STR_001", "Stream Creation");
define("CREATE_STR_002", "StreamID: ");
define("CREATE_STR_003", "StreamID automatically generated");
define("CREATE_STR_004", "Mapped to Stream");
define("CREATE_STR_005", "Enter the Map Stream ID");
define("CREATE_STR_006", "Stream Type");
define("CREATE_STR_007", "New Stream ");
define("CREATE_STR_008", " Add Source to exsiting Stream ");
define("CREATE_STR_009", "Main Stream");
define("CREATE_STR_010", "Select the Main Stream");
define("CREATE_STR_011", "StreamName");
define("CREATE_STR_012", "StreamCategory");
define("CREATE_STR_013", "Select the StreamCategory");
define("CREATE_STR_014", "Create this Stream "); 

// PeerPage
define("PEER_DWL_MSG_001", "Downloaded Stream list for peer ");
define("PEER_DWL_MSG_002", "Stream list downloaded from ");
define("PEER_DWL_MSG_003", "Stream list downloaded at ");
define("PEER_DWL_MSG_004", "Download command: ");



//NavBar
define("navbar_0", "Streams ");
define("navbar_0_001", "View All Streams ");
define("navbar_0_002", "Create New Stream ");
define("navbar_0_003", "Start All Streams ");
define("navbar_0_004", "Stop All Streams ");
define("navbar_0_005", "Check All Status ");
define("navbar_0_008", "Assign to Category ");


define("navbar_1", "Peers ");
define("navbar_1_001", "View All Peers ");
define("navbar_1_002", "Add Peers ");

define("navbar_2", "Users ");
define("navbar_2_001", "View All Users ");
define("navbar_2_002", "Add Users ");

define("navbar_3", "Settings ");

define("navbar_4", "Categories ");
define("navbar_4_001", "View All Categories ");
define("navbar_4_002", "Create New (Sub)Category ");
define("navbar_4_003", "All Assignments ");


