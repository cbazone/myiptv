<?php

class Controller{
	
	protected $Data;
	
	protected $Models;
	
	protected $Params;
	    
	public function getParams() 
	{
	  return $this->Params;
	}
	    
	public function getModels() 
	{
	  return $this->Models;
	}
	    
	public function getData() 
	{
	  return $this->Data;
	}
	
	public function __construct($data = array()) {
		$this->Data   = $data;
		$this->Params = App::getRouter()->getParams();

	}

	public function redirect($controller,$method = "index",$args = array(), $messages = array())
	{

	    $base_url = $this->GetBaseUrl();



	    if ($messages)
	    {
		    foreach ($messages as $message) 
		    {
		    	$this->SetSessionMessage($message);
	    	}	    	
	    }

	    if ($args)
	    {
	    	$location = $base_url . $controller. DS . $method . DS . implode(DS,$args);
	    } 
	    else
	    {
	    	$location = $base_url . $controller. DS . $method ;
	    }
	    

	    header("Location: " . $location);
	    exit;
	}	

	public function GetDbPath()
	{
		$db_name 	= Config::get('db_Name');
		$db_path 	= ROOT.DS.'db'.DS.$db_name;

		return $db_path;
	}		    

	public function GetServerOS()
	{
		$ServerOs = shell_exec('cat /etc/*-release');
		$DISTRIB_DESCRIPTION	= explode('DISTRIB_DESCRIPTION=',$ServerOs);
		$DISTRIB_DESCRIPTION 	= explode('NAME=', $DISTRIB_DESCRIPTION[1])[0];

		$NAME 				 	= explode('NAME="',$ServerOs);
		$NAME 				 	= explode('"',$NAME[1])[0];

		$ID_LIKE				= explode('ID_LIKE=',$ServerOs);
		$ID_LIKE				= explode('PRETTY_NAME=',$ID_LIKE[1])[0];

		$VERSION 				= explode('VERSION=',$ServerOs);
		$VERSION 				= explode('ID=',$VERSION[1])[0];

		$ServerOs = $DISTRIB_DESCRIPTION . "<br>" . $NAME . " ( " . $ID_LIKE . ") " . $VERSION;
		
		return $ServerOs;
	}

	public function GetServerKernel()
	{
		$ServerKernel = shell_exec('cat /proc/version');
		return $ServerKernel;
	}

	public function GetServerMemoryUsage()
	{
		$free 		= shell_exec('free');
		$free 		= (string)trim($free);
		$free_arr 	= explode("\n", $free);
		$mem 		= explode(" ", $free_arr[1]);
		$mem 		= array_filter($mem);
		$mem 		= array_merge($mem);
		$mem_usage 	= $mem[2]/1000;
	 
		return $mem_usage;
	}

	public function GetServerCpuUsage()
	{
		$load = sys_getloadavg();
		return $load[0];
	}

	public function GetUpTime()
	{
		$ut = strtok( exec( "cat /proc/uptime" ), "." );
		$days = sprintf( "%2d", ($ut/(3600*24)) );
		$hours = sprintf( "%2d", ( ($ut % (3600*24)) / 3600) );
		$min = sprintf( "%2d", ($ut % (3600*24) % 3600)/60  );
		$sec = sprintf( "%2d", ($ut % (3600*24) % 3600)%60  );
		return array( $days, $hours, $min, $sec );		
	}

	public function GetNetwork($device, $mode)
	{
		$cmd = "cat /sys/class/net/".$device."/statistics/".$mode;
		//echo $cmd . "<br>";
		$data = shell_exec($cmd);
		//echo $data . "<br>";
		return $data;
	}

	public function GetServerIP()
	{
		$ServerIP = $_SERVER["SERVER_ADDR"];
		return $ServerIP;
	}	

	public function ConvertByte($byte) { 
     
	    if($byte < 1024) 
	    { 
	        $ergebnis = round($byte, 2). ' Byte'; 
	    }
	    elseif($byte >= 1024 and $byte < pow(1024, 2)) 
	    { 
	        $ergebnis = round($byte/1024, 2).' KByte'; 
	    }elseif($byte >= pow(1024, 2) and $byte < pow(1024, 3)) 
	    { 
	        $ergebnis = round($byte/pow(1024, 2), 2).' MByte'; 
	    }elseif($byte >= pow(1024, 3) and $byte < pow(1024, 4)) 
	    { 
	        $ergebnis = round($byte/pow(1024, 3), 2).' GByte'; 
	    }elseif($byte >= pow(1024, 4) and $byte < pow(1024, 5)) 
	    { 
	        $ergebnis = round($byte/pow(1024, 4), 2).' TByte'; 
	    }elseif($byte >= pow(1024, 5) and $byte < pow(1024, 6)) 
	    { 
	        $ergebnis = round($byte/pow(1024, 5), 2).' PByte'; 
	    }elseif($byte >= pow(1024, 6) and $byte < pow(1024, 7)) 
	    { 
	        $ergebnis = round($byte/pow(1024, 6), 2).' EByte'; 
	    } 

		return $ergebnis; 
	}	

	public function GetPHPVersion()
	{
		$cmd = "php -v";
		$php_ver = shell_exec($cmd);
		$php_ver = explode('Copyright',$php_ver);
		return $php_ver[0];
	}

	public function GetPhpFPMVersion()
	{
		$cmd = "php5-fpm --version";
		$phpFPM_ver = shell_exec($cmd);

		if ($phpFPM_ver)
		{
    		$phpFPM_ver = explode('Copyright',$phpFPM_ver);
    		$phpFPM_ver = $phpFPM_ver[0];
		}
		else
		{
			$phpFPM_ver = "The program 'php5-fpm' is currently not installed; PHP managed by the WebServer";
		}
		
		return $phpFPM_ver;
	}	

	public function GetApacheVersion()
	{
		$cmd = "apache2ctl -v";
		$apache_ver = shell_exec($cmd);
		return $apache_ver;
	}	

	public function GetNginxVersion()
	{
		$cmd = "nginx -V";
		$nginx_ver = shell_exec($cmd);

		if (!$nginx_ver)
		{
			$nginx_ver = "NGINX is not installed";
		}

		return $nginx_ver;
	}	

	public function GetVLCVersion()
	{
		$cmd = "vlc --version";
		$vlc_ver = shell_exec($cmd);
		$vlc_ver = explode('This',$vlc_ver);

		return $vlc_ver[0];
	}	

	public function GetFFmpegVersion()
	{
		$cmd = "ffmpeg -version";
		$FFmpeg_ver = shell_exec($cmd);
		$FFmpeg_ver = explode('configuration',$FFmpeg_ver);

		return $FFmpeg_ver[0];
	}

	public function GetIPtablesVersion()
	{
		$cmd = "iptables --version";
		$IPtables_ver = shell_exec($cmd);

		return $IPtables_ver;
	}

	public function GetUFWVersion()
	{
		$cmd = "ufw --version";
		$UFW_ver = shell_exec($cmd);
		$UFW_ver = explode('Copyright',$UFW_ver);

		return $UFW_ver[0];
	}	

	public function UserGetNumberConnected()
	{
		//Function to know the number of distinct Users connected
		//  if a user is using 5 connexions it will only count for 1 user

		$apache_cmd = 'apachectl fullstatus';

		exec($apache_cmd, $output);

		$offset = 0;
		$length = 29;
		array_splice($output, $offset, $length);
		$UserConnections 	= array();

		$start 	= '/live/';
		$end	= '/';

		foreach ($output as $line) 
		{
			
			if (strpos($line, '_________') !== false) 
			{
    			break;
			}
			else
			{
				
				if (strpos($line, $start) !== false) 
				{
    				$UserConnections[] = $this->GetStringBetween($line, $start, $end);
				}
			}
		}

		$UserConnected 		 = array_unique($UserConnections);
		$NumberConnectedUser = count($UserConnected);

		return $NumberConnectedUser;
	}	

	public function StreamGetNumberConnected()
	{
		//Function to know the number of distinct streams requested
		//  if a stream is requested 5 times, it will only count for one.

		$apache_cmd = 'apachectl fullstatus';

		exec($apache_cmd, $output);

		$offset = 0;
		$length = 29;
		array_splice($output, $offset, $length);
		$StreamsConnections 	= array();

		foreach ($output as $line) 
		{
			
			if (strpos($line, '_________') !== false) 
			{
    			break;
			}
			else
			{
				
				if (strpos($line, '/live/') !== false) 
				{
    				$StreamsConnections[] = end((explode('/', $line)));
    				//$StreamsConnections[] = basename($line);
				}
			}
		}

		$StreamConnected 		= array_unique($StreamsConnections);
		$NumberConnectedStream 	= count($StreamConnected);

		return $NumberConnectedStream;
	}		

	public function UserGetNumberConnections($userID) 
	{
		//Function to know the number of connexions used by a user

		$db_path 	= $this->GetDbPath();
		$UserName 	= Users::GetUserName($db_path, $userID);
		$apache_cmd = 'apachectl fullstatus';

		exec($apache_cmd, $output);

		$offset = 0;
		$length = 29;
		$UserConnections 	= array();
		array_splice($output, $offset, $length);

		foreach ($output as $line) 
		{
			
			if (strpos($line, '_________') !== false) 
			{
    			break;
			}
			else
			{
				if (strpos($line, $UserName) !== false) 
				{
    				$UserConnections[] 	= $line;
    				//echo $UserConnections . "<br>";

				}
			}
		}

		$NumberConnectionsByUser = count($UserConnections);
		//$NumberConnectionsByUser = 1;

		//var_dump($NumberConnectionsByUser);
		Users::SetNumbConnections($db_path, $userID, $NumberConnectionsByUser);

		return $NumberConnectionsByUser;
	}

	public function StreamGetNumberConnections($StreamID)
	{
		//Function to know the number of connexions to a stream

		$apache_cmd = 'apachectl fullstatus';

		exec($apache_cmd, $output);

		$offset = 0;
		$length = 29;
		$StreamConnections 	= array();
		array_splice($output, $offset, $length);

		foreach ($output as $line) 
		{
			
			if (strpos($line, '_________') !== false) 
			{
    			break;
			}
			else
			{
				
				if (strpos($line, $StreamID) !== false) 
				{
    				$UserConnections 	= $line;
				}
			}
		}

		$NumberConnectionsByStream = count($StreamConnections);

		return $NumberConnectionsByStream;
	}	

	public function GetStringBetween($string, $start, $end)
	{
	    $string = ' ' . $string;
	    $ini = strpos($string, $start);
	    if ($ini == 0) return '';
	    $ini += strlen($start);
	    $len = strpos($string, $end, $ini) - $ini;

	    return substr($string, $ini, $len);
	}	

	public function SetSessionMessage($message)	
	{
		//$message_id 				= sha1(microtime(true));
		//var_dump($message);
		//echo "<br>";
		$_SESSION['message'][] 	= $message;
		//var_dump($_SESSION['message']);

		//return $message_id;
	}	

	public function GotMessages()
	{
		//global $_SESSION; 
		if( isset($_SESSION["message"]))  												
  		{ 
    		return true; 																
  		}
  			else  																	
  		{
    		return false; 																
  		}		
	}

	public function GetBaseUrl()
	{
		$uri 		= $_SERVER['REQUEST_URI'];
 		$base_url 	= Config::get('base_url');
 		$uri 		= str_replace($base_url, '', $uri);

 		$router = new Router($uri);
 		$lang	= $router->getLanguage();

 		$BaseUrl = $base_url . $lang . DS ;

 		return $BaseUrl;
	}	

	public function GetLangUrl($NewLang)
	{
		$uri_or 	= $_SERVER['REQUEST_URI'];
 		$base_url 	= Config::get('base_url');
 		$uri 		= str_replace($base_url, '', $uri_or);

 		$router 	= new Router($uri);
 		$lang		= $router->getLanguage();

 		$uri 		= str_replace($lang, $NewLang, $uri_or);

 		return $uri;
	}	

	public function SessionStarted()
	{
		if (session_status() == PHP_SESSION_NONE) {
		    session_start();
		}
	}

	public function CheckEngine()
	{
		$engine = Config::get('engine-cmd');

		return $engine;
	}	
} 