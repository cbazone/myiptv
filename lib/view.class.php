<?php

class View{
	
	protected $data;
	
	protected $path;
	
	protected static function getDefaultViewPath(){
		$router = App::getRouter();
		if (!$router){
			return false;
		}
		$controller_dir = $router->getController();
		
		if ($controller_dir == "users")
		{
			if ($router->getMethodPrefix().$router->getAction() == "download"
				OR $router->getMethodPrefix().$router->getAction() == "logout"
				OR $router->getMethodPrefix().$router->getAction() == "edituser" )
			{
				$template_name = "empty.html";
				$template = VIEWS_PATH.DS.$template_name;				
			}
			else
			{
				$template_name = $router->getMethodPrefix().$router->getAction().'.html';
				$template = VIEWS_PATH.DS.$controller_dir.DS.$template_name;
			}

		}
		elseif ($controller_dir == "peers") 
		{
			if ($router->getMethodPrefix().$router->getAction() == "checkstreamstatus"
				OR $router->getMethodPrefix().$router->getAction() == "editpeer"
				OR $router->getMethodPrefix().$router->getAction() == "ignorestream"
				OR $router->getMethodPrefix().$router->getAction() == "keepstream" )
			{
				$template_name = "empty.html";
				$template = VIEWS_PATH.DS.$template_name;				
			}
			else
			{
				$template_name = $router->getMethodPrefix().$router->getAction().'.html';
				$template = VIEWS_PATH.DS.$controller_dir.DS.$template_name;
			}
		}
		elseif($controller_dir == "streams")
		{
			if ($router->getMethodPrefix().$router->getAction() == "edit" 
				OR $router->getMethodPrefix().$router->getAction() == "create"
				OR $router->getMethodPrefix().$router->getAction() == "status"
				//OR $router->getMethodPrefix().$router->getAction() == "start"
				//OR $router->getMethodPrefix().$router->getAction() == "stop"
				OR $router->getMethodPrefix().$router->getAction() == "deletemapping"
				OR $router->getMethodPrefix().$router->getAction() == "fwuser"
				OR $router->getMethodPrefix().$router->getAction() == "addsourceform"
				OR $router->getMethodPrefix().$router->getAction() == "quality")
			{
				$template_name = "empty.html";
				$template = VIEWS_PATH.DS.$template_name;				
			}
			else
			{
				$template_name = $router->getMethodPrefix().$router->getAction().'.html';
				$template = VIEWS_PATH.DS.$controller_dir.DS.$template_name;
			}
		}
		elseif($controller_dir == "live")
		{
				$template_name = "empty.html";
				$template = VIEWS_PATH.DS.$template_name;			
		}
		else
		{
			$template_name = $router->getMethodPrefix().$router->getAction().'.html';
			$template = VIEWS_PATH.DS.$controller_dir.DS.$template_name;
		}
		
		return $template;
	}
	
	public function __construct($data = array(), $path = null) 
	{
		if ( !$path ){
			$path = self::getDefaultViewPath();
		}
		
		if ( !file_exists($path)){
			throw new Exception('Template file not found in ' .$path);
		}
		$this->path = $path;
		$this->data = $data;
	}
	
	public function render(){
		$data = $this->data;
		
		ob_start();
		include($this->path);
		$content = ob_get_clean();
		
		return $content;
	}

}