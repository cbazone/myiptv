<head>
	<meta charset="UTF-8">
	<title><?=Config::get('site_name')?></title>
	<!-- Bootstrap core CSS -->
	<!-- <link href="../../dist/css/bootstrap.min.css" rel="stylesheet"> -->
	
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<!-- <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->
	
	<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	<!-- <script src="../../assets/js/ie-emulation-modes-warning.js"></script> -->
	
	<!-- font-awesome - https://fortawesome.github.io/Font-Awesome/ -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	
	<!-- Custom styles for this template -->
	<link rel="stylesheet" type="text/css" href="/myiptv/css/jquery.multiselect.css" />
	<link rel="stylesheet" type="text/css" href="/myiptv/css/style.css" />
	<link rel="stylesheet" type="text/css" href="/myiptv/css/prettify.css" />
	<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" />
	
</head>
