<?php
define('DS', DIRECTORY_SEPARATOR);
$PATH = dirname(dirname(__FILE__));
define('ROOT', $PATH);
define('VIEWS_PATH', ROOT.DS.'views');

require_once(ROOT.DS.'lib'.DS.'init.php');

 $uri 		= $_SERVER['REQUEST_URI'];
 $base_url 	= Config::get('base_url');
 $uri 		= str_replace($base_url, '', $uri);


// $router = new Router($uri);


//print_r($PATH);

// echo '<PRE>';
// print_r('Route: ' .$router->getRoute(). PHP_EOL);
// print_r('Language: ' .$router->getLanguage(). PHP_EOL);
// print_r('Controller: ' .$router->getController(). PHP_EOL);
// print_r('Action to be called: ' .$router->getMethodPrefix().$router->getAction(). PHP_EOL);
// echo 'Params';
// print_r($router->getParams());

App::run($uri);