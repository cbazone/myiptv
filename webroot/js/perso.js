// <script type="text/javascript">
$(document).ready(function(){

	$("input[name$='inputCategoryType']").click(function()
	{
 		var selOption = $(this).val();
 		if (selOption === "NEW")
 		{
 			$("#ParentCategoryGroup").hide();
 			//$("#ParentCategoryGroup").prop( "disabled", true );
 		}

  	 	if (selOption === "SUB")
 		 {
 		 	$("#ParentCategoryGroup").show();
 		 	//$("#ParentCategoryGroup").prop( "disabled", false );
 		 }		
	});	

	$("input[name$='inputStreamType']").click(function()
	{
 		var selOption = $(this).val();
 		if (selOption === "NEW")
 		{
 			$("#inputExistingStream").hide();
 			$("#inputStreamCategory").show();
 		}

  	 	if (selOption === "EXIST")
 		 {
 		 	$("#inputExistingStream").show();
 		 	$("#inputStreamCategory").hide();
 		 }		
	});	

	$(".dropdown-menu li a").click(function()
	{
		  var selText = $(this).text();
		  $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
	});


    $("#InputUserCategory").multiselect();

// Manage de Menu
	if (window.location.href.indexOf("myiptv/users") > -1) 
	{
    	$("#menu_stream").removeClass('active');
    	$("#menu_peers").removeClass('active');
    	$("#menu_settings").removeClass('active');
    	$("#menu_users").addClass('active');
	}

	if (window.location.href.indexOf("myiptv/streams") > -1) 
	{
    	$("#menu_peers").removeClass('active');
    	$("#menu_settings").removeClass('active');
    	$("#menu_users").removeClass('active');
    	$("#menu_stream").addClass('active');
	}	

	if (window.location.href.indexOf("myiptv/peers") > -1) 
	{
    	$("#menu_settings").removeClass('active');
    	$("#menu_users").removeClass('active');
    	$("#menu_stream").removeClass('active');
    	$("#menu_peers").addClass('active');
	}		
  
	if (window.location.href.indexOf("myiptv/settings") > -1) 
	{
    	$("#menu_users").removeClass('active');
    	$("#menu_stream").removeClass('active');
    	$("#menu_peers").removeClass('active');
    	$("#menu_settings").addClass('active');
	}




});

window.onload = function(){
         $("#navbar > ul > li").hover(
          function(){ $(this).find("ul").slideDown('fast'); } ,
  function(){ $(this).find("ul").slideUp('fast'); } );
 }
// </script>

