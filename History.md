History
*******

### Version Beta 0.03 ###
* Added translation En/Fr, others are welcome
* Fix the ToolBar Menu (some links were not pointinh the baseUrl)
* Creation of the Unassigned Category by default
* Stream can be assigned to Category at creation
* Stream can be mapped at creation
* Flash Messages displayed after some action

### Version Beta 0.02 ###
* Check Number of Connection by Peer
* Check Max Number of Connections
* Check Number of running streams

### Version Beta 0.01 ###
* Restreaming using vlc
* Import Peer StreamList
* Mapping of PeerStreams to YourStream
* Manage Backup Streams, select best quality based on stream resolution
* Only tested on Ubuntu/Mint x64 with apache 2.4 (not tested on NGINX)